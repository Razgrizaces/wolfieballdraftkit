package wolfieball.data;

import wolfieball.mlb.MLBHitter;

public class FantasyHitter extends MLBHitter
{
    private int salary;
    private int contractInt;
    private String currentPosition;
    private String contract;
    private final int S1 = 1;
    private final int S2 = 2;
    private final int X = 0;
    private String fantasyTeamName;
    /**
     * Creates a FantasyHitter
     * @param f First Name
     * @param l Last Name
     * @param t Team
     * @param e Eligible?
     * @param n Nation of Birth
     * @param y Year of Birth
     * @param s Salary
     * @param ts isTaxiSquad?
     */
    public FantasyHitter(String f, String l, String t, boolean e, String n, int y, int s, String ftn) 
    {
        super(f, l, t, e, n, y);
        salary = s;
        fantasyTeamName = ftn;
        currentPosition = "";
        setIsFantasy(true);
    }
    public FantasyHitter()
    {
        super();
        salary = 0;
        fantasyTeamName = "New Fantasy Team";
        contract = "S2";
        currentPosition = "";
        contractInt = 2;
        setIsFantasy(true);
    }
    public FantasyHitter(MLBHitter h, int s, String ftn, int c)
    {
        super();
        loadFromHitter(h);
        salary = s;
        fantasyTeamName = ftn;
        currentPosition = "";
        determineContract(c);
        setIsFantasy(true);
    }
    public void setSalary(int bid)
    {
        salary = bid;
    }
    public int getSalary()
    {
        return salary;
    }
    public void setFantasyTeamName(String ftn)
    {
        fantasyTeamName = ftn;
    }
    public String getFantasyTeamName()
    {
        return fantasyTeamName;
    }
    public void calculatePoints()
    {
        //CODE TO CALCULATE POINTS
    }
    public boolean isTaxiSquad()
    {
        return false;
    }
    private void loadFromHitter(MLBHitter h)
    {
        setName(h.getFirstName(), h.getLastName());
        setAtBats(h.getAtBats());
        setRuns(h.getRuns());
        setHits(h.getHits());
        setHomeRuns(h.getHomeRuns());
        setRunsBattedIn(h.getRunsBattedIn());
        setStolenBases(h.getStolenBases());
        setTeam(h.getTeam());
        getBattingAverage();
        setIsFantasy(true);
        setIsHitter();
        setQualifyingPosition(h.getQualifyingPosition());
    }
    public int determineContract(int c)
    {
        if(c == 0)
        {
            contractInt = X;
            contract = "X";
        }
        if(c == 1)
        {
            contractInt = S1;
            contract = "S1";
        }
        if(c == 2)
        {
            contractInt = S2;
            contract = "S2";
        }
        if(c<0||c>2)
        {
            contractInt = X;
            contract = "X";
        }
        return contractInt;
    }
    public String getContractString(int c)
    {
        determineContract(c);
        return contract;
    }
    public int getContractInt()
    {
        return contractInt;
    }
    public String getCurrentPosition()
    {
        return currentPosition;
    }
    public void setCurrentPosition(String s)
    {
        currentPosition = s;
    }
}