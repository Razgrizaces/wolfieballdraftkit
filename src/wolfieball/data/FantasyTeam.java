package wolfieball.data;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class FantasyTeam
{
    private String name;
    private String ownerName;
    private int teamPoints;
    private int standing;
    private int MAX_SALARY = 260;
    private int currentSalary;
    private int NUM_ROSTER_PLAYERS = 23;
    private int NUM_TAXI_PLAYERS= 8;
    private int playersNeeded = 23;
    private int numTaxiPlayers;
    private int teamRuns;
    private int teamRunsBattedIn;
    private int teamHomeRuns;
    private int teamStolenBases;
    private int teamWins;
    private int teamStrikeouts;
    private int teamSaves;
    private int salaryPerPlayer;
    private double teamEarnedRunAverage;
    private double teamWalksHitsInningsPitched;
    private double teamBattingAverage;
    private ArrayList<FantasyPitcher> fantasyPitchers;
    private ArrayList<FantasyHitter> fantasyHitters;
    private ArrayList<FantasyPitcher> taxiRosterPitchers;
    private ArrayList<FantasyHitter> taxiRosterHitters;
    public FantasyTeam(String n, String on, ArrayList<FantasyPitcher> fp, ArrayList<FantasyHitter> fh)
    {
        name = n;
        ownerName = on;
        teamPoints = 0;
        standing = 0;
        currentSalary = 260;
        teamRuns = 0;
        teamRunsBattedIn = 0;
        numTaxiPlayers = 0;
        teamHomeRuns = 0;
        teamStolenBases = 0;
        teamWins = 0;
        teamStrikeouts = 0;
        teamSaves = 0;
        salaryPerPlayer = 0;
        teamEarnedRunAverage = 0;
        teamWalksHitsInningsPitched = 0; 
        teamBattingAverage = 0;
        fantasyPitchers =fp;
        fantasyHitters = fh;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the ownerName
     */
    public String getOwnerName() {
        return ownerName;
    }

    /**
     * @param ownerName the ownerName to set
     */
    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    /**
     * @return the teamPoints
     */
    public int getTeamPoints() {
        return teamPoints;
    }
    /**
     * @param teamPoints the teamPoints to set
     */
    public void setTeamPoints(int teamPoints) {
        this.teamPoints = teamPoints;
    }

    /**
     * @return the standing
     */
    public int getStanding() {
        return standing;
    }

    /**
     * @param standing the standing to set
     */
    public void setStanding(int standing) {
        this.standing = standing;
    }

    /**
     * @return the MAX_SALARY
     */
    public int getMAX_SALARY() {
        return MAX_SALARY;
    }

    /**
     * @return the currentSalary
     */
    public int getCurrentSalary() {
        return currentSalary;
    }
    /**
     * @param currentSalary the currentSalary to set
     */
    public void setCurrentSalary(int currentSalary) {
        this.currentSalary = currentSalary;
    }

    /**
     * @return the NUM_ROSTER_PLAYERS
     */
    public int getNUM_ROSTER_PLAYERS() {
        return NUM_ROSTER_PLAYERS;
    }

    /**
     * @return the NUM_TAXI_PLAYERS
     */
    public int getNUM_TAXI_PLAYERS() {
        return NUM_TAXI_PLAYERS;
    }

    /**
     * @return the playersNeeded
     */
    public int getPlayersNeeded() {
        return playersNeeded;
    }

    /**
     * @param playersNeeded the playersNeeded to set
     */
    public void setPlayersNeeded(int playersNeeded) {
        this.playersNeeded = playersNeeded;
    }

    /**
     * Obtains and sets teamRuns.
     * @return the team runs
     */
    public int getTeamRuns() {
        int tR = 0;
        for(FantasyHitter fh : fantasyHitters)
        {
            tR = tR + fh.getRuns();
        }
        teamRuns = tR;
        return teamRuns;
    }
    public int getTeamRunsBattedIn()
    {
        int tRBI = 0;
        for(FantasyHitter fh : fantasyHitters)
        {
            tRBI = tRBI + fh.getRunsBattedIn();
        }
        teamRunsBattedIn = tRBI;
        return teamRunsBattedIn;
    }
    /**
     * Obtains and sets teamHomeRuns.
     * @return the teamHomeRuns
     */
    public int getTeamHomeRuns() {
        int tHR = 0;
        for(FantasyHitter fh : fantasyHitters)
        {
            tHR = tHR + fh.getHomeRuns();
        }
        teamHomeRuns = tHR;
        return teamHomeRuns;
    }

    /**
     * Obtains and sets teamStolenBases
     * @return the teamStolenBases
     */
    public int getTeamStolenBases() {
        int tSB = 0;
        for(FantasyHitter fh : fantasyHitters)
        {
            tSB = tSB + fh.getStolenBases();
        }
        teamStolenBases = tSB;
        return teamStolenBases;
    }
    
    /**
     * Obtains and sets teamWins
     * @return the teamWins
     */
    public int getTeamWins() {
        int tW = 0;
        for(FantasyPitcher fp: fantasyPitchers)
        {
            tW = tW + fp.getWins();
        }
        teamWins = tW;
        return teamWins;
    }

    /**
     * Obtains and sets teamStrikeouts
     * @return the teamStrikeouts
     */
    public int getTeamStrikeouts() {
        int tK = 0;
        for(FantasyPitcher fp: fantasyPitchers)
        {
            tK = tK + fp.getStrikeouts();
        }
        teamStrikeouts = tK;
        return teamStrikeouts;
    }
    /**
     * Obtains and sets teamSaves
     * @return the teamSaves
     */
    public int getTeamSaves() {
        int tS = 0;
        for(FantasyPitcher fp: fantasyPitchers)
        {
            tS = tS + fp.getSaves();
        }
        teamSaves = tS;
        return teamSaves;
    }
    /**
     * Obtains and sets salaryPerPlayer
     * @return the salaryPerPlayer
     */
    public int getSalaryPerPlayer() {
        int spp;
        spp = currentSalary/playersNeeded;
        salaryPerPlayer = spp;
        return salaryPerPlayer;
    }
    /**
     * Obtains and sets earnedRunAverage
     * @return the teamEarnedRunAverage
     */
    public double getTeamEarnedRunAverage() {
        double tERA = 0;
        for(FantasyPitcher fp: fantasyPitchers)
        {
            tERA = tERA + fp.getERA();
        }
        if(fantasyPitchers.size()==0)
            return -1;
        tERA = (double)(tERA/fantasyPitchers.size());
        tERA = round(tERA, 2);
        teamEarnedRunAverage = tERA;
        return teamEarnedRunAverage;
    }

    /**
     * Obtains and sets teamWalksHitsInningsPitched
     * @return the teamWalksHitsInningsPitched
     */
    public double getTeamWalksHitsInningsPitched() {
        double tWHIP;
        double walksHits = 0;
        double inningsPitched = 0;
        for(FantasyPitcher fp : fantasyPitchers)
        {
            walksHits = walksHits + fp.getWalks() + fp.getHits();
            inningsPitched = inningsPitched + fp.getInningsPitched();
        }
        if(inningsPitched == 0)
            return -1;
        tWHIP = (double)(walksHits*9/inningsPitched);
        tWHIP = round(tWHIP, 2);
        teamWalksHitsInningsPitched = tWHIP;
        return teamWalksHitsInningsPitched;
    }

    /**
     * Obtains and sets teamBattingAverage
     * @return the teamBattingAverage
     */
    public double getTeamBattingAverage() {
        double tTBA;
        double teamHits = 0;
        double teamAtBats = 0;
        for(FantasyHitter fh : fantasyHitters)
        {
            //ONLY GETS SUM OF BATTING AVERAGE
            teamHits = teamHits + fh.getHits();
            teamAtBats = teamAtBats + fh.getAtBats();
        }
        //NEED TO CALCUALTE THE AVERAGE OF THE TEAM.
        if(teamAtBats==0)
            return -1;
        tTBA = (double)(teamHits/teamAtBats);
        tTBA = round(tTBA, 3);
        teamBattingAverage = tTBA;
        return teamBattingAverage;
    }
    /**
     * @return the fantasyPitchers
     */
    public ArrayList<FantasyPitcher> getFantasyPitchers() {
        return fantasyPitchers;
    }

    /**
     * @return the fantasyHitters
     */
    public ArrayList<FantasyHitter> getFantasyHitters() {
        return fantasyHitters;
    }
    
    public void setFantasyPitchers(ArrayList<FantasyPitcher> p)
    {
        if(fantasyPitchers == null)
            fantasyPitchers = new ArrayList<FantasyPitcher>();
        else
            fantasyPitchers.clear();
        fantasyPitchers.addAll(p);
    }
    public void setFantasyHitters(ArrayList<FantasyHitter> h)
    {
        if(fantasyHitters == null)
            fantasyHitters = new ArrayList<FantasyHitter>();
        else
            fantasyHitters.clear();
        fantasyHitters.addAll(h);
    }
    public void setPoints(int p)
    {
        teamPoints = p;
    }
    
    //NOW THE FUN PART, METHODS FOR ADDING PLAYERS AND RESTRICTING ADDING PLAYERS
    
    public boolean needsPlayers()
    {
        //HERE, WE CHECK HOW MANY PLAYERS THERE ARE
        if(playersNeeded==0)
            return false;
        else
            return true;
    }
    
    //CHECKS THROUGH THE FANTASY HITTERS AND PITCHERS TO CHECK IF THE PLAYERS ARE NEEDED
    public boolean isAvailable(String position)
    {
        if(!position.equals("P"))
        {
            int currentCatchers = 0;
            int currentOutfielders = 0;
            for(FantasyHitter h : fantasyHitters)
            {
                if(h.getCurrentPosition().equals("C")&&position.equals("C"))
                    currentCatchers ++;
                if(h.getCurrentPosition().equals("1B")&&position.equals("1B"))
                    return false;
                if(h.getCurrentPosition().equals("CI")&&position.equals("CI"))
                    return false;
                if(h.getCurrentPosition().equals("2B")&&position.equals("2B"))
                    return false;
                if(h.getCurrentPosition().equals("3B")&&position.equals("3B"))
                    return false;
                if(h.getCurrentPosition().equals("MI")&&position.equals("MI"))
                    return false;
                if(h.getCurrentPosition().equals("SS")&&position.equals("SS"))
                    return false;
                if(h.getCurrentPosition().equals("U")&&position.equals("U"))
                    return false;
                if(h.getCurrentPosition().equals("OF")&&position.equals("OF"))
                    currentOutfielders++;
            }
            if(position.equals("C"))
                return currentCatchers < 2;
            if(position.equals("OF"))
                return currentOutfielders <5;
            return true;
        }
        else
        {
            return fantasyPitchers.size() < 9;
        }
    }
    public boolean isNeeded(String position)
    {
        if(!position.equals("P"))
        {
            int currentCatchers = 0;
            int currentOutfielders = 0;
            for(FantasyHitter h : fantasyHitters)
            {
                if(h.getCurrentPosition().contains("C_"))
                    currentCatchers ++;
                if(h.getCurrentPosition().equals("1B"))
                    return false;
                if(h.getCurrentPosition().equals("CI"))
                    return false;
                if(h.getCurrentPosition().equals("3B"))
                    return false;
                if(h.getCurrentPosition().equals("MI"))
                    return false;
                if(h.getCurrentPosition().equals("SS"))
                    return false;
                if(h.getCurrentPosition().equals("U"))
                    return false;
                if(h.getCurrentPosition().equals("OF"))
                    currentOutfielders++;
            }
            if(position.equals("C"))
                return currentCatchers < 2;
            if(position.equals("OF"))
                return currentOutfielders <5;
            return true;
        }
        else
        {
            return fantasyPitchers.size() < 9;
        }
    }
    public boolean isTaxiRosterFull()
    {
        if(taxiRosterPitchers == null)
            taxiRosterPitchers = new ArrayList<FantasyPitcher>();
        if(taxiRosterHitters == null)
            taxiRosterHitters = new ArrayList<FantasyHitter>();
        return taxiRosterPitchers.size()+taxiRosterHitters.size() == 8;
    }
    public double round(double value, int places) {
        if (places < 0) 
            throw new IllegalArgumentException();
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    public void setTaxiRosterPitchers(ArrayList<FantasyPitcher> p)
    {
        if(taxiRosterPitchers == null)
            taxiRosterPitchers = new ArrayList<FantasyPitcher>();
        else
            taxiRosterPitchers.clear();
        taxiRosterPitchers.addAll(p);
    }
    public ArrayList<FantasyPitcher> getTaxiRosterPitchers()
    {
        return taxiRosterPitchers;
    }
    public ArrayList<FantasyHitter> getTaxiRosterHitters()
    {
        return taxiRosterHitters;
    }
    public void setTaxiRosterHitters(ObservableList<FantasyHitter> h)
    {
        if(taxiRosterHitters == null)
            taxiRosterHitters = new ArrayList<FantasyHitter>();
        else
            taxiRosterHitters.clear();
        taxiRosterHitters.addAll(h);
    }
}