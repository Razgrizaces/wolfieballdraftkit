package wolfieball.data;

import java.util.Comparator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import wolfieball.mlb.MLBHitter;
import wolfieball.mlb.MLBPitcher;
import wolfieball.mlb.MLBPlayer;

/*
The Data structure containing everything in the draft. 
This contains, currently at least:
MLBPlayers
FantasyTeams
*/
public class WolfieballDraft
{

    ObservableList<MLBPlayer> players;
    ObservableList<FantasyTeam> teams;
    ObservableList<MLBHitter> mlbHitters;
    ObservableList<MLBPitcher> mlbPitchers;
    ObservableList<MLBPlayer> draftedPlayers;
    ObservableList<FantasyHitter> draftedHitters;
    ObservableList<FantasyPitcher> draftedPitchers;
    
    public WolfieballDraft()
    {
        players = FXCollections.observableArrayList();
        teams = FXCollections.observableArrayList();
        mlbHitters = FXCollections.observableArrayList();
        mlbPitchers = FXCollections.observableArrayList();
        draftedPlayers = FXCollections.observableArrayList();
    }
    public WolfieballDraft(ObservableList<MLBPlayer> initPlayers, ObservableList<FantasyTeam> initTeams)
    {
        players = initPlayers;
        teams = initTeams;
    }
    public ObservableList<MLBPlayer> getMLBPlayers() {
        return players;
    }
    public ObservableList<FantasyTeam> getFantasyTeams()
    {
        return teams;
    }
    public void setMLBPlayers(ObservableList<MLBPlayer> setPlayers)
    {
        if(players == null)
            players = FXCollections.observableArrayList();
        else
            players.clear();
        players.addAll(setPlayers);
    }
    public void setMLBPlayers(ObservableList<MLBHitter> setHitters, ObservableList<MLBPitcher> setPitchers)
    {
        if(players == null)
           players = FXCollections.observableArrayList();
        else
            players.clear();
        players.addAll(setHitters);
        players.addAll(setPitchers);
    }
    public void setFantasyTeams(ObservableList<FantasyTeam> setTeams)
    {
        if(teams == null)
            teams = FXCollections.observableArrayList();
        else
            teams.clear();
        teams.addAll(setTeams);
    }
    public ObservableList<MLBPitcher> getMLBPitchers() {
        return mlbPitchers;
    }

    public ObservableList<MLBHitter> getMLBHitters() {
        return mlbHitters;
    }
    public void setMLBPitchers(ObservableList<MLBPitcher> p)
    {
        if(mlbPitchers==null)
            mlbPitchers = FXCollections.observableArrayList();
        else
            mlbPitchers.clear();
        mlbPitchers.addAll(p);
        players.clear();
    }
    public void setMLBHitters(ObservableList<MLBHitter> h)
    {
        if(mlbHitters==null)
            mlbHitters = FXCollections.observableArrayList();
        else
            mlbHitters.clear();
        mlbHitters.addAll(h);
    }
    public void setDraftedPlayers(ObservableList<MLBPlayer> p)
    {
        if(draftedPlayers==null)
            draftedPlayers = FXCollections.observableArrayList();
        else
            draftedPlayers.clear();
        draftedPlayers.addAll(p);
    }
    public void setDraftedHitters(ObservableList<FantasyHitter> h)
    {
        if(draftedHitters == null)
            draftedHitters = FXCollections.observableArrayList();
        else
            draftedHitters.clear();
        draftedHitters.addAll(h);
    }
    public void setDraftedPitchers(ObservableList<FantasyPitcher> p)
    {
        if(draftedPitchers == null)
            draftedPitchers = FXCollections.observableArrayList();
        else
            draftedPitchers.clear();
        draftedPitchers.addAll(p);
    }
    public void setDraftedPlayers()
    {
        if(draftedHitters == null)
            draftedHitters = FXCollections.observableArrayList();
        if(draftedPitchers == null)
            draftedPitchers = FXCollections.observableArrayList();
        if(draftedPlayers==null)
            draftedPlayers = FXCollections.observableArrayList();
        else
            draftedPlayers.clear();
        draftedPlayers.addAll(draftedHitters);
        draftedPlayers.addAll(draftedPitchers);
        FXCollections.sort(draftedPlayers, (playerA, playerB) -> playerA.getPickNumber() - playerB.getPickNumber());
    }
    public ObservableList<MLBPlayer> getDraftedPlayers()
    {
        if(draftedPlayers==null)
            draftedPlayers = FXCollections.observableArrayList();
        return draftedPlayers;
    }
    public ObservableList<FantasyHitter> getDraftedHitters()
    {
        if(draftedHitters == null)
            draftedHitters = FXCollections.observableArrayList();
        return draftedHitters;
    }
    public ObservableList<FantasyPitcher> getDraftedPitchers()
    {
        if(draftedPitchers == null)
            draftedPitchers = FXCollections.observableArrayList();
        return draftedPitchers;
    }
    
}
