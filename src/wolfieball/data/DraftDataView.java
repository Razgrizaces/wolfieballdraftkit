package wolfieball.data;

public interface DraftDataView {
    public void reloadDraft(WolfieballDraft draftToReload);
}
