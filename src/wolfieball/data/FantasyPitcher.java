package wolfieball.data;

import wolfieball.mlb.MLBPitcher;

public class FantasyPitcher extends MLBPitcher
{
    private int salary;
    private int contractInt;
    private String contract;
    private final int S1 = 1;
    private final int S2 = 2;
    private final int X = 0;
    private String fantasyTeamName;
    /**
     * Creates a FantasyPitcher
     * @param f First Name
     * @param l Last Name
     * @param t Team
     * @param e Eligible?
     * @param n Nation of Birth
     * @param y Year of Birth
     * @param s Salary
     * @param ts isTaxiSquad?
     */
    public FantasyPitcher(String f, String l, String t, boolean e, String n, int y, int s, String ftn) {
        super(f, l, t, e, n, y);
        salary = s;
        fantasyTeamName = ftn;
        contractInt = S2;
        contract = "S2";
        setIsFantasy(true);
    }
    public FantasyPitcher()
    {
        super();
        salary = 0;
        fantasyTeamName = "New Fantasy Team";
        contractInt = S2;
        contract = "S2";
        setIsFantasy(true);
    }
    public FantasyPitcher(MLBPitcher p, int s, String ftn, int c)
    {
        super();
        loadFromPitcher(p);
        salary = s;
        fantasyTeamName = ftn;
        determineContract(c);
        setIsFantasy(true);
    }
    private void loadFromPitcher(MLBPitcher p)
    {
        setName(p.getFirstName(), p.getLastName());
        setRole(p.getRole());
        setEarnedRuns(p.getEarnedRuns());
        setWins(p.getWins());
        setSaves(p.getSaves());
        setTeam(p.getTeam());
        setWalks(p.getWalks());
        setHits(p.getHits());
        setStrikeouts(p.getStrikeouts());
        getWHIP();
        getERA();
        setIsFantasy(true);
    }
    public void setSalary(int bid)
    {
        salary = bid;
    }
    public int getSalary()
    {
        return salary;
    }
    public void setFantasyTeamName(String ftn)
    {
        fantasyTeamName = ftn;
    }
    public String getFantasyTeamName()
    {
        return fantasyTeamName;
    }
    public void calculatePoints()
    {
        //CODE TO CALCULATE POINTS
    }
    public boolean isTaxiSquad()
    {
        return false;
    }
    public int determineContract(int c)
    {
        if(c == 0)
        {
            contractInt = X;
            contract = "X";
        }
        if(c == 1)
        {
            contractInt = S1;
            contract = "S1";
        }
        if(c == 2)
        {
            contractInt = S2;
            contract = "S2";
        }
        if(c<0||c>2)
        {
            contractInt = X;
            contract = "X";
        }
        return contractInt;
    }
    public String getContractString(int c)
    {
        determineContract(c);
        return contract;
    }
    public int getContractInt()
    {
        return contractInt;
    }
}