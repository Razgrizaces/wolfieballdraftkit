package wolfieball.data;

import wolfieball.data.WolfieballDraft;
import wolfieball.file.DraftFileManager;
import wolfieball.gui.Wolfieball_GUI;

public class DraftDataManager
{
    //DRAFT BEING EDITED
    WolfieballDraft draft;
    
    //UI, UPDATED WHENEVER THE MODEL'S DATA CHANGES
    DraftDataView view;
    
    //HELPS LOAD THINGS FOR DRAFT
    DraftFileManager fileManager;

    public DraftDataManager(DraftDataView initView) {
        view = initView;
    }
    
    //DO NOT NEED DEFAULT VALUES FOR COURSES
    //WE DO HOWEVER, NEED DEFAULT HITTERS.JSON AND PITCHERS.JSON

    public WolfieballDraft getDraft()
    {
        return draft;
    }
    
    public DraftFileManager getFileManager()
    {
        return fileManager;
    }
    public void reset() {
        //CLEARS DRAFT VALUES
    }
    
    //PRIVATE HELPER METHODS (GET HITTERS AND PITCHERS)

    public void setDraft(WolfieballDraft d) {
        draft = d;
    }
    
}