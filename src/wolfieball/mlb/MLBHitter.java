package wolfieball.mlb;

import java.math.BigDecimal;
import java.math.RoundingMode;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MLBHitter extends MLBPlayer implements Comparable
{
    private StringProperty qualifyingPosition;
    private IntegerProperty atBats;
    private IntegerProperty runs;
    private IntegerProperty hits;
    private IntegerProperty homeRuns;
    private IntegerProperty runsBattedIn;
    private IntegerProperty stolenBases;
    private DoubleProperty battingAverage;
    private String[] qualifyingPositions;
    public MLBHitter(String f, String l, String t, boolean e, String n, int y)
    {  
        super(f, l, t, e, n, y);
        isHitter = true;
        qualifyingPosition = new SimpleStringProperty("");
        atBats = new SimpleIntegerProperty(0);
        runs = new SimpleIntegerProperty(0);
        hits = new SimpleIntegerProperty(0);
        homeRuns = new SimpleIntegerProperty(0);
        runsBattedIn = new SimpleIntegerProperty(0);
        stolenBases = new SimpleIntegerProperty(0);
        battingAverage = new SimpleDoubleProperty(0);
    }

    public MLBHitter() {
        super();
        isHitter = true;
        qualifyingPosition = new SimpleStringProperty("");
        atBats = new SimpleIntegerProperty(0);
        runs = new SimpleIntegerProperty(0);
        hits = new SimpleIntegerProperty(0);
        homeRuns = new SimpleIntegerProperty(0);
        runsBattedIn = new SimpleIntegerProperty(0);
        stolenBases = new SimpleIntegerProperty(0);
        battingAverage = new SimpleDoubleProperty(0);
    }
    public IntegerProperty getAtBatsProperty()
    {
        return atBats;
    }
    public IntegerProperty getRunsProperty()
    {
        return runs;
    }
    public IntegerProperty getHitsProperty()
    {
        return hits;
    }
    public IntegerProperty getHomeRunsProperty()
    {
        return homeRuns;
    }
    public IntegerProperty getRunsBattedInProperty()
    {
        return runsBattedIn;
    }
    public IntegerProperty getStolenBasesProperty()
    {
        return stolenBases;
    }
    public DoubleProperty getBattingAverageProperty()
    {
        if(atBats.get() ==0)
        {
            battingAverage.set(0);
            return battingAverage;
        }
        battingAverage.set((double)hits.get()/(double)atBats.get());
        return battingAverage;
    }
    public StringProperty getQualifyingPositionProperty()
    {
        return qualifyingPosition;
    }
    public String getQualifyingPosition()
    {
        return qualifyingPosition.get();
    }
    public int getAtBats()
    {
        return atBats.get();
    }
    public int getRuns()
    {
        return runs.get();
    }
    public int getHits()
    {
        return hits.get();
    }
    public int getHomeRuns()
    {
        return homeRuns.get();
    }
    public int getRunsBattedIn()
    {
        return runsBattedIn.get();
    }
    public int getStolenBases()
    {
        return stolenBases.get();
    }
    public void setQualifyingPosition(String s)
    {
        qualifyingPosition.set(s);
    }
    public void setAtBats(int ab)
    {
        atBats.set(ab);
    }
    public void setRuns(int r)
    {
        runs.set(r);
    }
    public void setHits(int h)
    {
        hits.set(h);
    }
    public void setHomeRuns(int hr)
    {
        homeRuns.set(hr);
    }
    public void setRunsBattedIn(int rbi)
    {
        runsBattedIn.set(rbi);
    }
    public void setStolenBases(int sb)
    {
        stolenBases.set(sb);
    }
    public double getBattingAverage()
    {
        if(atBats.get() ==0)
            return 0;
        double ba = round((double)hits.get()/(double)atBats.get(), 3);
        battingAverage.set(ba);
        return battingAverage.get();
    }
    @Override
    public int compareTo(Object o) {
        return 0;
    }
    public boolean isHitter()
    {
        return true;
    }
    public double round(double value, int places) {
        if (places < 0) 
            throw new IllegalArgumentException();
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public String[] getQualifyingPositions()
    {
        String[] qps = new String[8];
        //ONLY ONE QUALIFYING POSITION
        if(!qualifyingPosition.get().contains("_"))
        {
            qps[0] = qualifyingPosition.get();
            if(qps[0].contains("1B")||qps[0].contains("3B"))
            {
                qps[1] = "CI";
                qps[2] = "U";
            }
            if(qps[0].contains("2B")||qps[0].contains("SS"))
            {
                qps[1] = "MI";
                qps[2] = "U";
            }
            qualifyingPositions = qps;
            return qualifyingPositions;
        }
        else
        {
            String temp = qualifyingPosition.get();
            int index = 0;
            while(temp.contains("_"))
            {
                //CHECKS THROUGH QUALIFYINGPOSITION STRING FOR POSITIONS
                qps[index] = temp.substring(0, temp.indexOf("_"));
                temp = temp.substring(temp.indexOf("_")+1);
                index++;
            }
            qps[index++] = temp;
            int count = 0;
            for(String s:qps)
                if(s!=null)
                    count++;
            String [] qpsTrimmed = new String[count];
            index = 0;
            for(String s:qps)
                if(s!=null)
                    qpsTrimmed[index++] = s;
            qualifyingPositions = qpsTrimmed;
            return qualifyingPositions;
        }
    }
    /**
     * Checks if the position is a qualifying position for the hitter.
     * @param filter the position to be checked.
     * @return a boolean representing if it's a qualifying position.
     */
    public boolean isQualifyingPosition(String filter) {
        qualifyingPositions = getQualifyingPositions();
        for(int i = 0; i <qualifyingPositions.length; i++)
        {
            if(filter.equals(qualifyingPositions[i]))
                return true;
        }
        return false;
    }
    public String getQualifyingPositionString(){
        getQualifyingPositions();
        String qps = "";
        for(String s: qualifyingPositions)
            if(s!=null)
                qps+=s+"_";
        qps = qps.substring(0, qps.length()-1);
        return qps;
    }
    public boolean getIsFantasy()
    {
        return isFantasy;
    }
    public void setIsFantasy(boolean fantasy)
    {
        isFantasy = fantasy;
    }
    public void setIsHitter()
    {
        isHitter= true;
    }
}