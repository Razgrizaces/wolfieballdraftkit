package wolfieball.mlb;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MLBPlayer 
{
    private String firstName;
    private String lastName;
    private String teamName;
    private String notes;
    private boolean eligible;
    private IntegerProperty pickNumber;
    boolean isHitter;
    boolean isFantasy;
    String NATION_OF_BIRTH;
    int YEAR_OF_BIRTH;
    public MLBPlayer()
    {
        firstName = "first";
        lastName = "last";
        teamName = "team";
        notes = "";
        eligible = false;
        isHitter = false;
        isFantasy = false;
        pickNumber = new SimpleIntegerProperty(0);
        NATION_OF_BIRTH = "USA";
        YEAR_OF_BIRTH = 2015;
    }
    public MLBPlayer(String f, String l, String t, boolean e, String n, int y)
    {
        firstName = f;
        lastName = l;
        teamName = t;
        eligible = e;
        notes = "";
        isHitter = false;
        isFantasy = false;
        NATION_OF_BIRTH = n;
        YEAR_OF_BIRTH = y;
    }
    public void setName(String f, String l)
    {
        firstName = f;
        lastName = l;
    }
    public void setTeam(String t)
    {
        teamName = t;
    }
    public void setNotes(String n )
    {
        notes = n;
    }
    public boolean checkIfEligible()
    {
        return eligible;
    }
    public String getFirstName()
    {
        return firstName;
    }
    public String getLastName()
    {
        return lastName;
    }
    public String getTeam()
    {
        return teamName;
    }
    public String getNotes()
    {
        return notes;
    }
    public boolean getIsHitter()
    {
        return isHitter;
    }
    public int getYearOfBirth()
    {
        return YEAR_OF_BIRTH;
    }
    public void setYearOfBirth(int y)
    {
        YEAR_OF_BIRTH = y;
    }
    public void setNationOfBirth(String n)
    {
        NATION_OF_BIRTH = n;
    }
    public String getNationOfBirth()
    {
        return NATION_OF_BIRTH;
    }
    public String getFullName()
    {
        return firstName + " " + lastName;
    }
    public void setPickNumber(int p)
    {
        pickNumber.set(p);
    }
    public int getPickNumber()
    {
        return pickNumber.get();
    }
    public void setPickNumberProperty(IntegerProperty p)
    {
        pickNumber = p;
    }
}