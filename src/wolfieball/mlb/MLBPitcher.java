package wolfieball.mlb;

import java.math.BigDecimal;
import java.math.RoundingMode;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MLBPitcher extends MLBPlayer implements Comparable
{
    private StringProperty role;
    private IntegerProperty earnedRuns;
    private IntegerProperty wins;
    private IntegerProperty saves;
    private IntegerProperty walks;
    private IntegerProperty hits;
    private IntegerProperty strikeouts;
    private DoubleProperty inningsPitched;
    private DoubleProperty walksHitsInningsPitched;
    private DoubleProperty earnedRunAverage;
    public MLBPitcher(String f, String l, String t, boolean e, String n, int y)
    {
        super(f, l, t, e, n, y);
        isHitter = false;
        role = new SimpleStringProperty("");
        inningsPitched = new SimpleDoubleProperty(0);
        earnedRuns = new SimpleIntegerProperty(0);
        wins = new SimpleIntegerProperty(0);
        saves = new SimpleIntegerProperty(0);
        walks = new SimpleIntegerProperty(0);
        hits = new SimpleIntegerProperty(0);
        strikeouts = new SimpleIntegerProperty(0);
        walksHitsInningsPitched = new SimpleDoubleProperty(0);
        earnedRunAverage = new SimpleDoubleProperty(0);
   }
    public MLBPitcher()
    {
        super();
        isHitter = false;
        role = new SimpleStringProperty("");
        inningsPitched = new SimpleDoubleProperty(0);
        earnedRuns = new SimpleIntegerProperty(0);
        wins = new SimpleIntegerProperty(0);
        saves = new SimpleIntegerProperty(0);
        walks = new SimpleIntegerProperty(0);
        hits = new SimpleIntegerProperty(0);
        strikeouts = new SimpleIntegerProperty(0);
        walksHitsInningsPitched = new SimpleDoubleProperty(0);
        earnedRunAverage = new SimpleDoubleProperty(0);
    }
    public DoubleProperty inningsPitchedProperty()
    {
        return inningsPitched;
    }
    public IntegerProperty getEarnedRunsProperty()
    {
        return earnedRuns;
    }
    public IntegerProperty getWalksProperty()
    {
        return walks;
    }
    public IntegerProperty getHitsProperty()
    {
        return hits;
    }
    public IntegerProperty getSavesProperty()
    {
        return saves;
    }
    public IntegerProperty getWinsProperty()
    {
        return wins;
    }
    public double getInningsPitched()
    {
        return inningsPitched.get();
    }
    public int getEarnedRuns()
    {
        return earnedRuns.get();
    }
    public int getWalks()
    {
        return walks.get();
    }
    public int getHits()
    {
        return hits.get();
    }
    public int getSaves()
    {
        return saves.get();
    }
    public int getWins()
    {
        return wins.get();
    }
    public double getWHIP()
    {
        if(inningsPitched.get() == 0)
            return 0;
        double whip = round(((double)(walks.get()+hits.get())/(double)inningsPitched.get()), 2);
        walksHitsInningsPitched.set(whip);
        return walksHitsInningsPitched.get();
    }
    public double getERA()
    {
        if(inningsPitched.get() == 0)
            return 0;
        double era = round(((double)(earnedRuns.get()*9)/(double)inningsPitched.get()), 2);
        earnedRunAverage.set(era);
        return earnedRunAverage.get();
    }
    public int getStrikeouts()
    {
        return strikeouts.get();
    }
    public String getRole()
    {
        return role.get();
    }
    public void setEarnedRuns(int er)
    {
        earnedRuns.set(er);
    }
    public void setWalks(int w)
    {
        walks.set(w);
    }
    public void setHits(int h)
    {
        hits.set(h);
    }
    public void setWins(int w)
    {
        wins.set(w);
    }
    public void setSaves(int s)
    {
        saves.set(s);
    }
    public void setInningsPitched(double ip)
    {
        inningsPitched.set(ip);
    }
    public void setStrikeouts(int k)
    {
        strikeouts.set(k);
    }
    public void setRole(String r)
    {
        role.set(r);
    }
    @Override
    public int compareTo(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public double round(double value, int places) {
        if (places < 0) 
            throw new IllegalArgumentException();
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    public boolean getIsFantasy()
    {
        return isFantasy;
    }
    public void setIsFantasy(boolean fantasy)
    {
        isFantasy = fantasy;
    }
}