package wolfieball.mlb;

import java.util.ArrayList;

public class MLBTeam
{
    private ArrayList<MLBPitcher> pitchers;
    private ArrayList<MLBHitter> hitters;
    private String name;
    //DEFINES THE TEAMS, HITTERS AND PITCHERS.
    public MLBTeam(ArrayList<MLBPitcher> p, ArrayList<MLBHitter> h, String s)
    {
        pitchers = p;
        hitters = h;
        name = s;
    }
    public MLBTeam(String s)
    {
        pitchers = new ArrayList<MLBPitcher>();
        hitters = new ArrayList<MLBHitter>();
        name = s;
    }
    public MLBTeam()
    {
        pitchers = new ArrayList<MLBPitcher>();
        hitters = new ArrayList<MLBHitter>();
        name = "Name";
    }
    /**
     * @return the pitchers
     */
    public ArrayList<MLBPitcher> getPitchers() {
        return pitchers;
    }

    /**
     * @param pitchers the pitchers to set
     */
    public void setPitchers(ArrayList<MLBPitcher> pitchers) {
        this.pitchers = pitchers;
    }

    /**
     * @return the hitters
     */
    public ArrayList<MLBHitter> getHitters() {
        return hitters;
    }

    /**
     * @param hitters the hitters to set
     */
    public void setHitters(ArrayList<MLBHitter> hitters) {
        this.hitters = hitters;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
}