package wolfieball.file;

import wolfieball.data.FantasyTeam;
import wolfieball.data.TeamStanding;
import wolfieball.data.WolfieballDraft;
import wolfieball.draft.TaxiDraft;
import wolfieball.draft.BiddingDraft;
import wolfieball.mlb.MLBTeam;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.ObservableList;
import wolfieball.mlb.MLBHitter;
import wolfieball.mlb.MLBPitcher;

/**
 * This interface provides an abstraction of what a file manager should do. Note
 * that file managers know how to read and write courses, instructors, and subjects,
 * but now how to export sites.
 * 
 * @author Richard McKenna
 */
public interface DraftFileManager {
    public void loadDraft(WolfieballDraft draftToLoad, String draftPath) throws IOException;
    public TaxiDraft loadTaxiDraft(String draftPath);
    public BiddingDraft loadBiddingDraft(String draftPath);
    public TeamStanding loadTeamStandings(String draftPath);
    public ObservableList<FantasyTeam> loadFantasyTeams(String draftPath);
    public ObservableList<MLBHitter> loadMLBHitters(String draftPath);
    public ObservableList<MLBPitcher> loadMLBPitchers(String draftPath);
    public void saveDraft(WolfieballDraft wd) throws IOException;
    public void saveTaxiDraft(TaxiDraft td, String draftPath);
    public void saveBiddingDraft(BiddingDraft bd, String draftPath);
    public void saveTeamStandings(TeamStanding ts, String draftPath);
    public void saveFantasyTeams(List<Object> teams, String draftPath);
    public void saveMLBTeams(List<Object> teams, String draftPath);
}
