package wolfieball.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import wolfieball.data.FantasyTeam;
import wolfieball.data.TeamStanding;
import wolfieball.data.WolfieballDraft;
import wolfieball.draft.BiddingDraft;
import wolfieball.draft.TaxiDraft;
import wolfieball.mlb.MLBTeam;
import wolfieball.mlb.MLBHitter;
import wolfieball.mlb.MLBPitcher;


public class JsonDraftFileManager implements DraftFileManager
{
    
    //JSON VARIABLES FOR ALL THE BIG DATA PARTS
    
    private String JSON_DRAFT = "draft";
    private String JSON_TEAM_STANDINGS = "teamStandings";
    private String JSON_FANTASY_TEAMS = "fantasyTeams";
    private String JSON_MLB_TEAMS = "MLBTeams";
    private String JSON_BIDDING_DRAFT = "biddingDraft";
    private String JSON_TAXI_DRAFT = "taxiDraft";
    private String JSON_EXT = ".json";
    private String SLASH = "/"; 

    //JSON VARIABLES FOR THE MLB TEAM 
    
    //MAIN ONES FOR THE FILES, BECAUSE THEY'RE ARRAYS
    private String JSON_HITTERS = "Hitters";
    private String JSON_PITCHERS = "Pitchers";
    //SHARED BETWEEN PLAYER
    private String JSON_MLB_TEAM_NAME = "TEAM";
    private String JSON_MLB_PLAYER_FIRST_NAME = "FIRST_NAME";
    private String JSON_MLB_PLAYER_LAST_NAME = "LAST_NAME";
    private String JSON_MLB_PLAYER_HITS = "H";
    private String JSON_MLB_PLAYER_NOTES = "NOTES";
    private String JSON_MLB_PLAYER_YEAR_OF_BIRTH = "YEAR_OF_BIRTH";
    private String JSON_MLB_PLAYER_NATION_OF_BIRTH = "NATION_OF_BIRTH";
    
    //HITTER ONLY
    private String JSON_MLB_PLAYER_QUALIFYING_POSITION = "QP";
    private String JSON_MLB_PLAYER_RUNS = "R";
    private String JSON_MLB_PLAYER_AT_BATS = "AB";
    private String JSON_MLB_PLAYER_HOME_RUNS = "HR";
    private String JSON_MLB_PLAYER_RUNS_BATTED_IN = "RBI";
    private String JSON_MLB_PLAYER_STOLEN_BASES = "SB";
    
    //PITCHER ONLY
    private String JSON_MLB_PLAYER_WINS = "W";
    private String JSON_MLB_PLAYER_SAVES = "SV";
    private String JSON_MLB_PLAYER_INNINGS_PITCHED = "IP";
    private String JSON_MLB_PLAYER_EARNED_RUNS = "ER";
    private String JSON_MLB_PLAYER_BASE_ON_BALLS = "BB";//AKA: WALKS
    private String JSON_MLB_PLAYER_STRIKEOUTS = "K";
    
    
    //JSON VARIABLES FOR THE FANTASY TEAM
    
    private String JSON_FANTASY_TEAM_NAME = "fantasyTeamName";
    private String JSON_FANTASY_PLAYER_FIRST_NAME = "fantasyPlayerFirstName";
    private String JSON_FANTASY_PLAYER_LAST_NAME = "fantasyPlayerLastName";
    private String JSON_FANTASY_TEAM_OWNER = "fantasyTeamOwnerName";
    //JSON VARIABLES FOR TEAM STANDINGS
    
    //ALL LOADING METHODS
    @Override
    public void loadDraft(WolfieballDraft draftToLoad, String draftPath) throws IOException
    {
        JsonObject json = loadJSONFile(draftPath);
        //SET DRAFT VARIABLES TO THE VARIABLES FOUND IN THE JSON ARRAY.
    }

    @Override
    public TaxiDraft loadTaxiDraft(String draftPath) {
        return null;
    }

    @Override
    public BiddingDraft loadBiddingDraft(String draftPath) {
         return null;
    }

    @Override
    public TeamStanding loadTeamStandings(String draftPath) {
         return null;
    }

    @Override
    public ObservableList<FantasyTeam> loadFantasyTeams(String draftPath) {
         return null;
    }

    @Override
    public ObservableList<MLBHitter> loadMLBHitters(String draftPath) {
        ArrayList<MLBTeam> teams = new ArrayList<MLBTeam>();
        try {
            return loadHitters(teams, draftPath);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public ObservableList<MLBPitcher> loadMLBPitchers(String draftPath) {
       ArrayList<MLBTeam> teams = new ArrayList<MLBTeam>();
        try {
            return loadPitchers(teams, draftPath);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    //ALL SAVING METHODS 
    @Override
    public void saveDraft(WolfieballDraft wd) {
    }

    @Override
    public void saveTaxiDraft(TaxiDraft td, String draftPath) {
    }

    @Override
    public void saveBiddingDraft(BiddingDraft bd, String draftPath) {
    }

    @Override
    public void saveTeamStandings(TeamStanding ts, String draftPath) {
    }

    @Override
    public void saveFantasyTeams(List<Object> teams, String draftPath) {
    }

    @Override
    public void saveMLBTeams(List<Object> teams, String draftPath) {
    }
    //CREATING NECESSARY JSON OBJECTS/ARRAYS
    
    private JsonObject makeDraftJsonObject(WolfieballDraft d)
    {
         return null;
    }
    private JsonObject makeTeamStandingsJsonObject(TeamStanding ts)
    {
         return null;
    }
    private JsonArray makeFantasyTeamsJsonArray(List<Object> ft)
    {
         return null;
    }
    private JsonArray makeMLBTeamsJsonArray(List<Object> mt)
    {
         return null;
    }
    private JsonObject makeBiddingDraftJsonObject(BiddingDraft bd)
    {
         return null;
    }
    private JsonObject makeTaxiDraftJsonObject(TaxiDraft td)
    {
         return null;
    }
    
    //HERE ARE PRIVATE HELPER METHODS TO HELP PUBLIC METHODS
      // LOADS A JSON FILE AS A SINGLE OBJECT AND RETURNS IT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }    
    
    // LOADS AN ARRAY OF A SPECIFIC NAME FROM A JSON FILE AND
    // RETURNS IT AS AN ArrayList FULL OF THE DATA FOUND
    private ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        ArrayList<String> items = new ArrayList();
        JsonArray jsonArray = json.getJsonArray(arrayName);
        for (JsonValue jsV : jsonArray) {
            items.add(jsV.toString());
        }
        return items;
    }
    
    //MAKES AND RETURNS A JSON OBJECT FOR THE PROVIDED DRAFT
    private JsonObject makeDraftItemJsonObject(WolfieballDraft Draft)
    {
        return null;   
    }
    
    //BUILDS AND RETURNS THE DRAFT FOUND IN THE JSON OBJECT
    
    //LOADS THE HITTERS.JSON
    private ObservableList<MLBHitter> loadHitters(ArrayList<MLBTeam> teams, String filePath) throws IOException
    {
        //HOW SHOULD WE PROCESS IT?... BY TEAM? ARRAYLIST<TEAMS>?
        
        //TAKE THE JSON FILE
        ArrayList<String> hitterStrings = loadArrayFromJSONFile(filePath, "Hitters");
        ArrayList<String> cleanedHitters = new ArrayList<String>();
        ArrayList<MLBHitter> arrayHitters = new ArrayList<MLBHitter>();
        for(String s: hitterStrings)
        {
            s = s.replaceAll("\"", "");
            cleanedHitters.add(s);
        }
        for(String s: cleanedHitters)
        {
           //LOAD EACH VALUE INTO A PLAYER
            MLBHitter h = new MLBHitter();
            parseStringIntoHitters(h, s);
            arrayHitters.add(h);
        }
        ObservableList<MLBHitter> hitters = FXCollections.observableArrayList(arrayHitters);
        return hitters;
    }
    
    //LOAD PITCHERS.JSON
    private ObservableList<MLBPitcher> loadPitchers(ArrayList<MLBTeam> teams, String filePath) throws IOException
    {
        ArrayList<String> pitcherStrings = loadArrayFromJSONFile(filePath, JSON_PITCHERS);
        ArrayList<String> cleanedPitchers =  new ArrayList<String>();
        //LOAD EACH VALUE INTO A PLAYER
        ArrayList<MLBPitcher> arrayPitchers = new ArrayList<MLBPitcher>();
        for(String s: pitcherStrings)
        {
            s = s.replaceAll("\"", "");
            cleanedPitchers.add(s);
        }
        for(String s: cleanedPitchers)
        {
            MLBPitcher p = new MLBPitcher();
            parseStringIntoPitchers(p, s);
            arrayPitchers.add(p);
        }
        ObservableList<MLBPitcher> pitchers = FXCollections.observableArrayList(arrayPitchers);
        return pitchers;
    }
    private void parseStringIntoHitters(MLBHitter h, String s)
    {
        //OBTAIN ALL THE STRINGS FROM JSON
        String team = s.substring(s.indexOf(JSON_MLB_TEAM_NAME + ":")+5, s.indexOf(JSON_MLB_PLAYER_LAST_NAME)-1);
        String lastName = s.substring(s.indexOf(JSON_MLB_PLAYER_LAST_NAME)+10, s.indexOf(JSON_MLB_PLAYER_FIRST_NAME)-1);
        String firstName = s.substring(s.indexOf(JSON_MLB_PLAYER_FIRST_NAME)+11, s.indexOf(JSON_MLB_PLAYER_QUALIFYING_POSITION)-1);
        String qualifyingPosition = s.substring(s.indexOf(JSON_MLB_PLAYER_QUALIFYING_POSITION)+3, s.indexOf(JSON_MLB_PLAYER_AT_BATS)-1);
        String atBats = s.substring(s.indexOf(JSON_MLB_PLAYER_AT_BATS)+3, s.indexOf("," +JSON_MLB_PLAYER_RUNS));
        String runs = s.substring(s.indexOf(","+ JSON_MLB_PLAYER_RUNS)+3, s.indexOf("," + JSON_MLB_PLAYER_HITS));
        String hits = s.substring(s.indexOf("," + JSON_MLB_PLAYER_HITS)+3, s.indexOf(","+JSON_MLB_PLAYER_HOME_RUNS));
        String homeRuns = s.substring(s.indexOf("," + JSON_MLB_PLAYER_HOME_RUNS)+4, s.indexOf(JSON_MLB_PLAYER_RUNS_BATTED_IN)-1);
        String runsBattedIn = s.substring(s.indexOf(JSON_MLB_PLAYER_RUNS_BATTED_IN)+4, s.indexOf(JSON_MLB_PLAYER_STOLEN_BASES)-1);
        String stolenBases = s.substring(s.indexOf(JSON_MLB_PLAYER_STOLEN_BASES)+3, s.indexOf(JSON_MLB_PLAYER_NOTES)-1);
        String notes = s.substring(s.indexOf(JSON_MLB_PLAYER_NOTES)+6, s.indexOf(JSON_MLB_PLAYER_YEAR_OF_BIRTH)-1);
        String yearOfBirth = s.substring(s.indexOf(JSON_MLB_PLAYER_YEAR_OF_BIRTH)+14, s.indexOf(JSON_MLB_PLAYER_NATION_OF_BIRTH)-1);
        String nationOfBirth = s.substring(s.indexOf(JSON_MLB_PLAYER_NATION_OF_BIRTH)+16, s.indexOf("}"));
        
        if(qualifyingPosition.contains("1B")||qualifyingPosition.contains("3B"))
            qualifyingPosition +="_CI";
        if(qualifyingPosition.contains("2B")||qualifyingPosition.contains("SS"))
            qualifyingPosition += "_MI";
        qualifyingPosition+="_U";
        //THEN WE SET THE VALUES.
        h.setTeam(team);
        h.setName(firstName, lastName);
        h.setQualifyingPosition(qualifyingPosition);
        h.setAtBats(Integer.parseInt(atBats));
        h.setRuns(Integer.parseInt(runs));
        h.setHits(Integer.parseInt(hits));
        h.setHomeRuns(Integer.parseInt(homeRuns));
        h.setRunsBattedIn(Integer.parseInt(runsBattedIn));
        h.setStolenBases(Integer.parseInt(stolenBases));
        h.setNotes(notes);
        h.setYearOfBirth(Integer.parseInt(yearOfBirth));
        h.setNationOfBirth(nationOfBirth);
        h.getBattingAverageProperty();
    }
    private void parseStringIntoPitchers(MLBPitcher p, String s)
    {
        String team = s.substring(s.indexOf(JSON_MLB_TEAM_NAME + ":")+5, s.indexOf(JSON_MLB_PLAYER_LAST_NAME)-1);
        String lastName = s.substring(s.indexOf(JSON_MLB_PLAYER_LAST_NAME)+10, s.indexOf(JSON_MLB_PLAYER_FIRST_NAME)-1);
        String firstName = s.substring(s.indexOf(JSON_MLB_PLAYER_FIRST_NAME)+11, s.indexOf(JSON_MLB_PLAYER_INNINGS_PITCHED)-1);
        String inningsPitched = s.substring(s.indexOf(JSON_MLB_PLAYER_INNINGS_PITCHED)+3, s.indexOf(JSON_MLB_PLAYER_EARNED_RUNS)-1);
        String earnedRuns = s.substring(s.indexOf(JSON_MLB_PLAYER_EARNED_RUNS)+3, s.indexOf("," +JSON_MLB_PLAYER_WINS));
        String wins = s.substring(s.indexOf(","+JSON_MLB_PLAYER_WINS)+3, s.indexOf(","+JSON_MLB_PLAYER_SAVES));
        String saves = s.substring(s.indexOf(","+ JSON_MLB_PLAYER_SAVES)+4, s.indexOf("," + JSON_MLB_PLAYER_HITS));
        String hits = s.substring(s.indexOf("," + JSON_MLB_PLAYER_HITS)+3, s.indexOf(","+JSON_MLB_PLAYER_BASE_ON_BALLS));
        String walks = s.substring(s.indexOf(JSON_MLB_PLAYER_BASE_ON_BALLS)+3, s.indexOf(","+JSON_MLB_PLAYER_STRIKEOUTS));
        String strikeouts = s.substring(s.indexOf("," +JSON_MLB_PLAYER_STRIKEOUTS)+3, s.indexOf(JSON_MLB_PLAYER_NOTES)-1);
        String notes = s.substring(s.indexOf("," +JSON_MLB_PLAYER_NOTES)+7, s.indexOf(JSON_MLB_PLAYER_YEAR_OF_BIRTH)-1);
        String yearOfBirth = s.substring(s.indexOf(JSON_MLB_PLAYER_YEAR_OF_BIRTH)+14, s.indexOf(JSON_MLB_PLAYER_NATION_OF_BIRTH)-1);
        String nationOfBirth = s.substring(s.indexOf(JSON_MLB_PLAYER_NATION_OF_BIRTH)+16, s.indexOf("}"));
        
        //THEN WE SET THE VALUES.
        p.setTeam(team);
        p.setName(firstName, lastName);
        p.setWins(Integer.parseInt(wins));
        p.setInningsPitched(Double.parseDouble(inningsPitched));
        p.setEarnedRuns(Integer.parseInt(earnedRuns));
        p.setSaves(Integer.parseInt(saves));
        p.setHits(Integer.parseInt(hits));
        p.setWalks(Integer.parseInt(walks));
        p.setStrikeouts(Integer.parseInt(strikeouts));
        p.setNotes(notes);
        p.setRole(notes);
        p.setYearOfBirth(Integer.parseInt(yearOfBirth));
        p.setNationOfBirth(nationOfBirth);
    }
}