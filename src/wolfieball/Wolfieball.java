package wolfieball;

import static com.sun.javafx.sg.prism.NGCanvas.PATH_BASE;
import wolfieball.gui.*;
import wolfieball.file.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static wolfieball.WolfieballStartupConstants.PATH_DATA;
import static wolfieball.WolfieballStartupConstants.PROPERTIES_FILE_NAME;
import static wolfieball.WolfieballStartupConstants.PROPERTIES_SCHEMA_FILE_NAME;
import wolfieball.data.DraftDataManager;
import xml_utilities.InvalidXMLFileFormatException;

public class Wolfieball extends Application {
    // THIS IS THE FULL USER INTERFACE, WHICH WILL BE INITIALIZED
    // AFTER THE PROPERTIES FILE IS LOADED
   Wolfieball_GUI gui;

    /**
     * This is where our Application begins its initialization, it will
     * create the GUI and initialize all of its components.
     * 
     * @param primaryStage This application's window.
     */
    @Override
    public void start(Stage primaryStage) throws IOException {
        
        // LOAD APP SETTINGS INTO THE GUI AND START IT UP
        boolean success = loadProperties();
        if(success)
        {
            //PropertiesManager props = PropertiesManager.getPropertiesManager();
            String appTitle = "Wolfieball Draft Kit";
            JsonDraftFileManager jsonFileManager = new JsonDraftFileManager();
            
            DraftExporter exporter = new DraftExporter("sites/base/");
            gui = new Wolfieball_GUI(primaryStage); 
            gui.setDraftFileManager(jsonFileManager);
            gui.setDraftExporter(exporter);
            
            DraftDataManager dataManager = new DraftDataManager(gui);
            gui.setDataManager(dataManager);
            gui.initGUI(appTitle);
        }
    }
    
    /**
     * Loads this application's properties file, which has a number of settings
     * for initializing the user interface.
     * 
     * @return true if the properties file was loaded successfully, false otherwise.
     */
    public boolean loadProperties() {
        return true;
//           try {
//            // LOAD THE SETTINGS FOR STARTING THE APP
//            PropertiesManager props = PropertiesManager.getPropertiesManager();
//            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
//            props.loadProperties(PROPERTIES_FILE_NAME, PROPERTIES_SCHEMA_FILE_NAME);
//            return true;
//       } catch (InvalidXMLFileFormatException ixmlffe) {
//            // SOMETHING WENT WRONG INITIALIZING THE XML FILE
//            return false;
//        }        
    }

    /**
     * This is where program execution begins. Since this is a JavaFX app
     * it will simply call launch, which gets JavaFX rolling, resulting in
     * sending the properly initialized Stage (i.e. window) to our start
     * method in this class.
     */
    public static void main(String[] args) {
        launch(args);
    }
}
