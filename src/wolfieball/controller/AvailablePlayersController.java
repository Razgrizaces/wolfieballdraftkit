/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wolfieball.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import wolfieball.data.DraftDataManager;
import wolfieball.data.FantasyHitter;
import wolfieball.data.FantasyPitcher;
import wolfieball.data.FantasyTeam;
import wolfieball.data.WolfieballDraft;
import wolfieball.gui.AddPlayerDialog;
import wolfieball.gui.EditPlayerDialog;
import wolfieball.gui.Wolfieball_GUI;
import wolfieball.mlb.MLBHitter;
import wolfieball.mlb.MLBPitcher;
import wolfieball.mlb.MLBPlayer;

/**
 *
 * @author Tristen
 */
public class AvailablePlayersController {
    FantasyHitter fantasyHitter;
    FantasyPitcher fantasyPitcher;
    
    AddPlayerDialog apd;
    EditPlayerDialog epd;
    
    public AvailablePlayersController(Stage initPrimaryStage, 
                                      WolfieballDraft draft)
    {
        epd = new EditPlayerDialog(initPrimaryStage, draft);
        apd = new AddPlayerDialog(initPrimaryStage, draft);
    }
    
    public void handleEditPlayerRequest(Wolfieball_GUI gui, MLBPlayer playerToEdit)
    {
        DraftDataManager ddm = gui.getDataManager();
        WolfieballDraft draft = ddm.getDraft();
        ObservableList<FantasyTeam> fantasyTeams = draft.getFantasyTeams();
        epd.showEditPlayerDialog(playerToEdit);
        if(epd.wasCompleteSelected())
        {
            MLBPlayer player = epd.getPlayer();
            MLBHitter hitter;
            MLBPitcher pitcher;
            ObservableList<MLBHitter> mlbHitters = FXCollections.observableArrayList(draft.getMLBHitters());
            ObservableList<MLBPitcher> mlbPitchers = FXCollections.observableArrayList(draft.getMLBPitchers());
            //DETERMINE WHAT THE PLAYER IS AND ADD IT TO THE FANTASY TEAM
            if(player.getIsHitter())
            {
                //USE THIS TO REMOVE PLAYERS AFTER WE'RE DONE WITH THEM
                
                hitter = (MLBHitter)player;
                //INITIALIZING THE VARIABLES FOR THE FANTASY PLAYER
                fantasyHitter = new FantasyHitter(hitter, 0, "", 0);
                if(epd.getSelectedFantasyTeamString()==null)
                {
                    gui.showErrorDialog("You did not select a fantasy team. No player was added.");
                    return;
                }
                fantasyHitter.setFantasyTeamName(epd.getSelectedFantasyTeamString());
                //IF THE ROSTER IS FULL
                if(epd.getFantasyTeam(fantasyHitter.getFantasyTeamName()).getPlayersNeeded()==0)
                {
                    if(epd.getFantasyTeam(fantasyPitcher.getFantasyTeamName()).isTaxiRosterFull())
                    {
                        gui.showErrorDialog("The Taxi Roster is full.");
                        return;
                    }
                    fantasyHitter.setSalary(1);
                    fantasyHitter.setCurrentPosition(fantasyHitter.getQualifyingPositions()[0]);
                    fantasyHitter.getContractString(0);
                    fantasyHitter.setYearOfBirth(player.getYearOfBirth());
                    fantasyHitter.setNationOfBirth(player.getNationOfBirth());
                    //EXIT, FIX OTHER THINGS, AND THEN EXIT
                    FantasyTeam editedFT = epd.getFantasyTeam(fantasyHitter.getFantasyTeamName());
                    editedFT.getTaxiRosterHitters().add(fantasyHitter);
                    editedFT.setCurrentSalary(editedFT.getCurrentSalary()-1);
                    int index = 0;
                    for(FantasyTeam ft: fantasyTeams)
                    {
                        if(ft.getName().equals(fantasyHitter.getFantasyTeamName()))
                        {
                            fantasyTeams.set(index, editedFT);
                            break;
                        }
                        index++;
                    }
                    //REMOVES PLAYER FROM MLB PLAYERS
                    for(MLBPitcher p: mlbPitchers)
                    {
                        if(hitter.getFullName().equals(p.getFullName()))
                        {
                            mlbPitchers.remove(hitter);
                            break;
                        }
                    }
                    draft.setMLBPitchers(mlbPitchers);
                    return;
                }
                if(epd.getSelectedPosition()!=null)
                {
                    if(epd.getFantasyTeam(fantasyHitter.getFantasyTeamName()).isAvailable(epd.getSelectedPosition()))
                    {
                         fantasyHitter.setCurrentPosition(epd.getSelectedPosition());
                    }
                    else
                    {
                        gui.showErrorDialog("The position is not available. The player was not edited.");
                        return;
                    }
                }
                else
                {
                    gui.showErrorDialog("You did not select a position. No player was edited.");
                    return;
                }
                if(epd.getSalary()!=0)
                    fantasyHitter.setSalary(epd.getSalary());
                else
                    return;
                //SETS CONTRACT
                if(epd.getContract()!=null)
                {
                    int contract = 0;
                    if(epd.getContract().equals("S1"))
                        contract = 1;
                    if(epd.getContract().equals("S2"))
                        contract = 2;
                    fantasyHitter.getContractString(contract);
                }
                else
                {
                     
                }
                //OTHER VARIABLES THAT MAY HAVE NOT BEEN SET BECAUSE IT'S A NEW PLAYER
                fantasyHitter.setYearOfBirth(player.getYearOfBirth());
                fantasyHitter.setNationOfBirth(player.getNationOfBirth());
                //ADDS PLAYER TO FANTASY TEAM
                if(epd.getFantasyTeam(fantasyHitter.getFantasyTeamName())!=null)
                {
                    epd.getFantasyTeam(fantasyHitter.getFantasyTeamName()).getFantasyHitters().add(fantasyHitter);
                }
                else
                {
                    gui.showErrorDialog("You did not select a team. No player was added.");
                    return;
                }
                if(fantasyHitter.getContractString(fantasyHitter.getContractInt()).equals("S2"))
                {
                    draft.getDraftedHitters().add(fantasyHitter);
                    draft.setDraftedPlayers();
                    int size = draft.getDraftedPlayers().size();
                    fantasyHitter.setPickNumber(size);
                }
                FantasyTeam editedFT = epd.getFantasyTeam(fantasyHitter.getFantasyTeamName());
                editedFT.setCurrentSalary(editedFT.getCurrentSalary()-epd.getSalary());
                editedFT.setPlayersNeeded(editedFT.getPlayersNeeded()-1);
                int index = 0;
                //ADDS PLAYER TO FANTASY TEAMS
                for(FantasyTeam ft: fantasyTeams)
                {
                    if(ft.getName().equals(fantasyHitter.getFantasyTeamName()))
                    {
                        fantasyTeams.set(index, editedFT);
                        break;
                    }
                    index++;
                }
                for(MLBHitter h: mlbHitters)
                {
                    if(hitter.getFullName().equals(h.getFullName()))
                    {
                        mlbHitters.remove(hitter);
                        break;
                    }
                }
                draft.setMLBHitters(mlbHitters);
            }
            //PLAYER IS A PITCHER
            else
            {
                pitcher = (MLBPitcher)player;
                fantasyPitcher = new FantasyPitcher(pitcher, 0, "", 0);
                fantasyPitcher.setFantasyTeamName(epd.getSelectedFantasyTeamString());
                if(epd.getFantasyTeam(fantasyPitcher.getFantasyTeamName()).getPlayersNeeded()==0)
                {
                    if(epd.getFantasyTeam(fantasyPitcher.getFantasyTeamName()).isTaxiRosterFull())
                    {
                        gui.showErrorDialog("The Taxi Roster is full.");
                        return;
                    }
                    fantasyPitcher.setSalary(1);
                    fantasyPitcher.setRole("P");
                    fantasyPitcher.getContractString(0);
                    fantasyPitcher.setYearOfBirth(player.getYearOfBirth());
                    fantasyPitcher.setNationOfBirth(player.getNationOfBirth());
                    //EXIT, FIX OTHER THINGS, AND THEN EXIT
                    FantasyTeam editedFT = epd.getFantasyTeam(fantasyPitcher.getFantasyTeamName());
                    editedFT.getTaxiRosterPitchers().add(fantasyPitcher);
                    editedFT.setCurrentSalary(editedFT.getCurrentSalary()-1);
                    int index = 0;
                    for(FantasyTeam ft: fantasyTeams)
                    {
                        if(ft.getName().equals(fantasyPitcher.getFantasyTeamName()))
                        {
                            fantasyTeams.set(index, editedFT);
                            break;
                        }
                        index++;
                    }
                    //REMOVES PLAYER FROM MLB PLAYERS
                    for(MLBPitcher p: mlbPitchers)
                    {
                        if(pitcher.getFullName().equals(p.getFullName()))
                        {
                            mlbPitchers.remove(pitcher);
                            break;
                        }
                    }
                    draft.setMLBPitchers(mlbPitchers);
                    return;
                }
                if(epd.getSelectedPosition()!=null)
                {
                    if(epd.getFantasyTeam(fantasyPitcher.getFantasyTeamName()).isAvailable(epd.getSelectedPosition()))
                         fantasyPitcher.setRole(epd.getSelectedPosition());
                    else
                    {
                        gui.showErrorDialog("The position is not available. The player was not edited.");
                        return;
                    }
                }
                if(epd.getSalary()!=0)
                    fantasyPitcher.setSalary(epd.getSalary());
                if(epd.getContract()!=null)
                {
                    int contract = 0;
                    if(epd.getContract().equals("S1"))
                        contract = 1;
                    if(epd.getContract().equals("S2"))
                        contract = 2;
                    fantasyPitcher.getContractString(contract);
                }
                else
                {
                    gui.showErrorDialog("You did not select a contract. No player was added.");
                    return;
                }
                if(epd.getSelectedPosition()!=null)
                    fantasyPitcher.setRole(epd.getSelectedPosition());
                fantasyPitcher.setYearOfBirth(player.getYearOfBirth());
                fantasyPitcher.setNationOfBirth(player.getNationOfBirth());
                //ADDS PLAYER TO FANTASY TEAM
                epd.getFantasyTeam(fantasyPitcher.getFantasyTeamName()).getFantasyPitchers().add(fantasyPitcher);
                if(fantasyPitcher.getContractString(fantasyPitcher.getContractInt()).equals("S2"))
                {
                    draft.getDraftedPitchers().add(fantasyPitcher);
                    draft.setDraftedPlayers();
                    int size = draft.getDraftedPlayers().size();
                    fantasyPitcher.setPickNumber(size);
                }
                FantasyTeam editedFT = epd.getFantasyTeam(fantasyPitcher.getFantasyTeamName());
                editedFT.setCurrentSalary(editedFT.getCurrentSalary()-epd.getSalary());
                editedFT.setPlayersNeeded(editedFT.getPlayersNeeded()-1);
                int index = 0;
                for(FantasyTeam ft: fantasyTeams)
                {
                    if(ft.getName().equals(fantasyPitcher.getFantasyTeamName()))
                    {
                        fantasyTeams.set(index, editedFT);
                        break;
                    }
                    index++;
                }
                //REMOVES PLAYER FROM MLB PLAYERS
                for(MLBPitcher p: mlbPitchers)
                {
                    if(pitcher.getFullName().equals(p.getFullName()))
                    {
                        mlbPitchers.remove(pitcher);
                        break;
                    }
                }
                draft.setMLBPitchers(mlbPitchers);
            }
        }
        else{
            //USER PRESSED CANCEL, DO NOTHING
        }
    }
}
