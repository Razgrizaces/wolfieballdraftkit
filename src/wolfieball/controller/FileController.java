package wolfieball.controller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import javafx.stage.Stage;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;
import static wolfieball.WolfieballStartupConstants.PATH_DRAFT;
import static wolfieball.Wolfieball_PropertyType.DRAFT_SAVED_MESSAGE;
import static wolfieball.Wolfieball_PropertyType.NEW_DRAFT_CREATED_MESSAGE;
import static wolfieball.Wolfieball_PropertyType.SAVE_UNSAVED_WORK_MESSAGE;
import wolfieball.data.DraftDataManager;
import wolfieball.data.WolfieballDraft;
import wolfieball.file.DraftFileManager;
import wolfieball.file.DraftExporter;
import wolfieball.gui.YesNoCancelDialog;
import wolfieball.gui.MessageDialog;
import wolfieball.gui.ProgressDialog;
import wolfieball.gui.Wolfieball_GUI;


public class FileController
{
    // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    private boolean saved;
    
    // THIS GUY KNOWS HOW TO READ AND WRITE DRAFT DATA
    private DraftFileManager draftIO;
    
    // THIS WILL PROVIDE FEEDBACK TO THE USER AFTER
    // WORK BY THIS CLASS HAS COMPLETED
    MessageDialog messageDialog;
    
    //USE TO ASK YES/NO/CANCEL QUESTIONS
    YesNoCancelDialog yesNoCancelDialog;
    
    //GET VERIFICATION FEEDBACK
    private PropertiesManager properties;
    
    public FileController(
            DraftFileManager initDraftIO,
            DraftExporter initExporter,
            MessageDialog initMessageDialog,
            YesNoCancelDialog initYesNoCancelDialog)
    {
        //NOTHING YET
        saved = true;
        
        //KEEP THESE GUYS FOR LATER
        draftIO = initDraftIO;
        
        //PROVIDE FEEDBACK
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
        properties = PropertiesManager.getPropertiesManager();
        
    }
    
    public void markAsEdited(Wolfieball_GUI gui) {
        // THE Draft OBJECT IS NOW DIRTY
        saved = false;
        
        // LET THE UI KNOW
        gui.updateToolbarControls(saved);
    }
    /**
     * This method starts the process of editing a new Draft. If a Draft is
     * already being edited, it will prompt the user to save it first.
     * 
     * @param gui The user interface editing the Draft.
     */
    public void handleNewDraftRequest(Wolfieball_GUI gui) {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave(gui);
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW Draft
            if (continueToMakeNew) {
                DraftDataManager dataManager = gui.getDataManager();
                dataManager.reset();
                saved = false;

                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                gui.updateToolbarControls(saved);
                
                // TELL THE USER THE DRAFT HAS BEEN CREATED
                messageDialog.show("New draft created, but not saved.");
            }
        } catch (IOException ioe) {
            // SOMETHING WENT WRONG, PROVIDE FEEDBACK
            
        }
    }

    /**
     * This method lets the user open a Draft saved to a file. It will also
     * make sure data for the current Draft is not lost.
     * 
     * @param gui The user interface editing the Draft.
     */
    public void handleOpenDraftRequest(Wolfieball_GUI gui) {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToOpen = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToOpen = promptToSave(gui);
            }

            // IF THE USER REALLY WANTS TO OPEN A Draft
            if (continueToOpen) {
                // GO AHEAD AND PROCEED LOADING A Draft
                promptToOpen(gui);
            }
        } catch (IOException ioe) {
            // SOMETHING WENT WRONG
            
        }
    }

    /**
     * This method will save the current Draft to a file. Note that we already
     * know the name of the file, so we won't need to prompt the user.
     * 
     * @param gui The user interface editing the Draft.
     * 
     * @param DraftToSave The Draft being edited that is to be saved to a file.
     */
    public void handleSaveDraftRequest(Wolfieball_GUI gui, WolfieballDraft draftToSave) {
        try {
            // SAVE IT TO A FILE
            draftIO.saveDraft(draftToSave);

            // MARK IT AS SAVED
            saved = true;

            // TELL THE USER THE FILE HAS BEEN SAVED
            messageDialog.show(properties.getProperty(DRAFT_SAVED_MESSAGE));

            // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
            // THE APPROPRIATE CONTROLS
            gui.updateToolbarControls(saved);
        } catch (IOException ioe) {
            
        }
    }

    /**
     * This method will export the current Draft.
     * 
     * @param gui
     */
    public void handleExportDraftRequest(Wolfieball_GUI gui) {
        // EXPORT THE DRAFT
        DraftDataManager dataManager = gui.getDataManager();
        WolfieballDraft DraftToExport = dataManager.getDraft();

        // TO EXPORT DRAFT, ALL WE NEED TO DO IS JUST SET
        // THE SCREEN TO FANTASY TEAMS
        
        //SETS TEAM TO FANTASY TEAMS
    }

    /**
     * This method will exit the application, making sure the user doesn't lose
     * any data first.
     * 
     * @param gui
     */
    public void handleExitRequest(Wolfieball_GUI gui) {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToExit = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE
                continueToExit = promptToSave(gui);
            }

            // IF THE USER REALLY WANTS TO EXIT THE APP
            if (continueToExit) {
                // EXIT THE APPLICATION
                System.exit(0);
            }
        } catch (IOException ioe) {
        }
    }
    private boolean promptToSave(Wolfieball_GUI gui) throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show("Would you like to save your unsaved work?");
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) {
            // SAVE THE DRAFT
            DraftDataManager dataManager = gui.getDataManager();
            draftIO.saveDraft(dataManager.getDraft());
            saved = true;
            
            //NOT NECESSARY FOR INSTRUCTORS
            //MAY BE SO FOR HITTERS.JSON AND PITCHERS.JSON.
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (selection.equals(YesNoCancelDialog.CANCEL)) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }

    /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     */
    private void promptToOpen(Wolfieball_GUI gui) {
        // AND NOW ASK THE USER FOR THE DRAFT TO OPEN
        FileChooser draftFileChooser = new FileChooser();
        draftFileChooser.setInitialDirectory(new File("./data/draft/"));
        File selectedFile = draftFileChooser.showOpenDialog(gui.getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
                WolfieballDraft draftToLoad = gui.getDataManager().getDraft();
                draftIO.loadDraft(draftToLoad, selectedFile.getAbsolutePath());
                gui.reloadDraft(draftToLoad);
                saved = true;
                gui.updateToolbarControls(saved);
                
            } catch (Exception e) {
                //error handling
            }
        }
    }

    /**
     * This mutator method marks the file as not saved, which means that when
     * the user wants to do a file-type operation, we should prompt the user to
     * save current work first. Note that this method should be called any time
     * the draft is changed in some way.
     */
    public void markFileAsNotSaved() {
        saved = false;
    }

    /**
     * Accessor method for checking to see if the current Draft has been saved
     * since it was last edited.
     *
     * @return true if the current Draft is saved to the file, false otherwise.
     */
    public boolean isSaved() {
        return saved;
    }
}