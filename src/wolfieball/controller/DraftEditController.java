package wolfieball.controller;

/**
     * 
     * Controller class handles all responses to draft
     * editing input, including verification of data and
     * binding of entered data to the Draft object.
     * 
     */
public class DraftEditController
{
    //USE THIS TO MAKE SURE OUR PROGRAMMED UPDATES OF UI
    //VALUES DON'T TRIGGER THEMSELVES
    private boolean enabled;
    
    /**
     * 
     * Constructor that gets this controller ready.
     * 
     */
    public DraftEditController()
    {
        enabled = true;
    }
    public void enable(boolean enableSetting)
    {
        enabled = enableSetting;
    }
    public void handlePlayersRequest()
    {
        //this one is the biggest, will need load of hitters and pitchers.
        //only deals with data though.
    }
    public void handleFantasyRequest()
    {
        
    }
    
}