/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wolfieball.controller;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import wolfieball.data.DraftDataManager;
import wolfieball.data.FantasyHitter;
import wolfieball.data.FantasyPitcher;
import wolfieball.data.FantasyTeam;
import wolfieball.data.WolfieballDraft;
import wolfieball.gui.EditFantasyPlayerDialog;
import wolfieball.gui.FantasyTeamDialog;
import wolfieball.gui.RemoveTeamDialog;
import wolfieball.gui.Wolfieball_GUI;
import wolfieball.mlb.MLBHitter;
import wolfieball.mlb.MLBPitcher;
import wolfieball.mlb.MLBPlayer;

/**
 *
 * @author Tristen
 */
public class FantasyTeamsController {
    
    FantasyTeamDialog aftd;
    RemoveTeamDialog rtd;
    EditFantasyPlayerDialog epd;
    
    FantasyHitter fantasyHitter;
    FantasyPitcher fantasyPitcher;
    
    public FantasyTeamsController(Stage initPrimaryStage, 
                                      WolfieballDraft draft)
    {
        aftd = new FantasyTeamDialog(initPrimaryStage, draft);
        rtd = new RemoveTeamDialog(initPrimaryStage);
        epd = new EditFantasyPlayerDialog(initPrimaryStage, draft);
    }
    
    public void handleAddFantasyTeams(Wolfieball_GUI gui)
    {
        DraftDataManager ddm = gui.getDataManager();
        WolfieballDraft draft = ddm.getDraft();
        FantasyTeam teamToAdd = aftd.showAddFantasyTeamDialog(draft.getFantasyTeams());
        if(aftd.wasCompleteSelected())
        {
            if(teamToAdd==null)
            {
                gui.showErrorDialog("You did not input a name for either the owner or team. Please try again.");
                return;
            }
            draft.getFantasyTeams().add(teamToAdd);
            gui.getFileController().markAsEdited(gui);
        }
    }
    public void handleEditFantasyTeam(Wolfieball_GUI gui, FantasyTeam teamToEdit)
    {
        aftd.showEditFantasyTeamDialog(teamToEdit);
        if(aftd.wasCompleteSelected())
        {
            FantasyTeam ft = aftd.getFantasyTeam();
            
            //TAKES ALL THE FANTASY PLAYERS
            ArrayList<FantasyPitcher> fantasyPitchers = new ArrayList<FantasyPitcher>();
            ArrayList<FantasyHitter> fantasyHitters = new ArrayList<FantasyHitter>();
            
            //ADDS THEM TO THE LIST OF FANTASY PLAYERS IN NEW TEAM
            fantasyHitters.addAll(ft.getFantasyHitters());
            fantasyPitchers.addAll(ft.getFantasyPitchers());
            
            //SETS NAME AND VARIABLES OF THE TEAM
            teamToEdit.setName(ft.getName());
            teamToEdit.setOwnerName(ft.getOwnerName());
            teamToEdit.setFantasyHitters(fantasyHitters);
            teamToEdit.setFantasyPitchers(fantasyPitchers);
            
            //MARKS IT AS DIRTY, BECAUSE IT'S NOT SAVED.
            gui.getFileController().markAsEdited(gui);
        }
    }
    public void handleEditFantasyPlayer(Wolfieball_GUI gui, MLBPlayer playerToEdit)
    {
        DraftDataManager ddm = gui.getDataManager();
        WolfieballDraft draft = ddm.getDraft();
        ObservableList<FantasyTeam> fantasyTeams = draft.getFantasyTeams();
        epd.showEditPlayerDialog(playerToEdit);
        
        if(epd.wasCompleteSelected())
        {
            MLBPlayer player = epd.getPlayer();
            FantasyHitter hitter;
            FantasyPitcher pitcher;
            if(player.getIsHitter())
            {
                //USE THIS TO REMOVE PLAYERS AFTER WE'RE DONE WITH THEM
                hitter = (FantasyHitter)player;
                //INITIALIZING THE VARIABLES FOR THE FANTASY PLAYER
                fantasyHitter = new FantasyHitter(hitter, 0, hitter.getFantasyTeamName(), 0);
                //REMOVE FROM PLAYERS, ADD TO AVAILABLE PLAYERS.
                if(epd.getSelectedFantasyTeamString().equals("<Available Players>"))
                {
                    FantasyTeam changePlayer = obtainFantasyTeam(fantasyTeams);
                    int index = obtainFantasyTeamIndex(fantasyTeams);
                    //REMOVES PLAYER AND PUTS IT IN AVAAILABLE PLAYERS AREA
                    int currentSalary = changePlayer.getCurrentSalary()+hitter.getSalary();
                    int playersNeeded =changePlayer.getPlayersNeeded()+1;
                    changePlayer.setCurrentSalary(currentSalary);
                    changePlayer.setPlayersNeeded(playersNeeded);
                    changePlayer.getFantasyHitters().remove(hitter);
                    draft.getDraftedHitters().remove(hitter);
                    draft.setDraftedPlayers();
                    handleRemoveDraftedPlayer(draft);
                    fantasyHitter.setYearOfBirth(player.getYearOfBirth());
                    fantasyHitter.setNationOfBirth(player.getNationOfBirth());
                    draft.getMLBHitters().add(fantasyHitter);
                    fantasyTeams.set(index, changePlayer);
                    //RETURNS, BECAUSE NOTHING ELSE REALLY MATTERS, YOU'RE REMOVING THE PLAYER
                    return;
                }
                //NAME IS THE SAME, WE DON'T CHANGE IT'S TEAM
                if(epd.getSelectedFantasyTeamString().equals(fantasyHitter.getFantasyTeamName()))
                {
                    //BUT WE SHOULD REMOVE THE PLAYER AS TO AVOID DUPLICATES
                    FantasyTeam changePlayer = obtainFantasyTeam(fantasyTeams);
                    if(epd.getSelectedPosition()!=null)
                    {
                        if(epd.getFantasyTeam(fantasyHitter.getFantasyTeamName()).isAvailable(epd.getSelectedPosition()))
                             fantasyHitter.setCurrentPosition(epd.getSelectedPosition());
                        else
                        {
                            gui.showErrorDialog("The position is not available. The player was not edited.");
                            return;
                        }
                    }
                    draft.getDraftedHitters().remove(hitter);
                    draft.setDraftedPlayers();
                    handleRemoveDraftedPlayer(draft);
                    int index = obtainFantasyTeamIndex(fantasyTeams);
                    //WE'LL NEED TO UPDATE THE SALARY/PLAYERS NEEDED BECAUSE WE CHANGE THAT LATER
                    int currentSalary = changePlayer.getCurrentSalary()+hitter.getSalary();
                    int playersNeeded =changePlayer.getPlayersNeeded()+1;
                    changePlayer.setCurrentSalary(currentSalary);
                    changePlayer.setPlayersNeeded(playersNeeded);
                    changePlayer.getFantasyHitters().remove(hitter);
                    //TEAM SHOULD HAVE X NUMBER OF PLAYERS SET, SO DON'T WORRY ABOUT INCREMENTING NUM PLAYERS
                    fantasyTeams.set(index, changePlayer);
                    //ADDING THE PLAYER HAPPENS LATER ON, SO DON'T WORRY ABOUT IT
                }
                //NAME IS SOMETHING ELSE, SO WE REMOVE THE PLAYER AND THEN PUT IT INTO THE OTHER TEAM.
                else
                {
                    FantasyTeam removePlayer = obtainFantasyTeam(fantasyTeams);
                    int currentSalary = removePlayer.getCurrentSalary()+hitter.getSalary();
                    int playersNeeded = removePlayer.getPlayersNeeded()+1;
                    removePlayer.setCurrentSalary(currentSalary);
                    removePlayer.setPlayersNeeded(playersNeeded);
                    draft.getDraftedHitters().remove(hitter);
                    draft.setDraftedPlayers();
                    handleRemoveDraftedPlayer(draft);
                    int index = obtainFantasyTeamIndex(fantasyTeams);
                    fantasyHitter.setFantasyTeamName(epd.getSelectedFantasyTeamString());
                    if(epd.getSelectedPosition()!=null)
                    {
                        if(epd.getFantasyTeam(fantasyHitter.getFantasyTeamName()).isAvailable(epd.getSelectedPosition()))
                             fantasyHitter.setCurrentPosition(epd.getSelectedPosition());
                        else
                        {
                            gui.showErrorDialog("The position is not available. The player was not edited.");
                            return;
                        }
                    }
                    removePlayer.getFantasyHitters().remove(hitter);
                    fantasyTeams.set(index, removePlayer);
                    //ADDING THE PLAYER HAPPENS LATER ON, SO DON'T WORRY ABOUT IT
                }
                
                if(epd.getSalary()!=0)
                    fantasyHitter.setSalary(epd.getSalary());
                else
                    return;
                //SETS CONTRACT
                if(epd.getContract()!=null)
                {
                    int contract = 0;
                    if(epd.getContract().equals("S1"))
                        contract = 1;
                    if(epd.getContract().equals("S2"))
                        contract = 2;
                    fantasyHitter.getContractString(contract);
                }
                else
                {
                    gui.showErrorDialog("You did not select a contract. No player was added.");
                    return;
                }
                if(fantasyHitter.getContractString(fantasyHitter.getContractInt()).equals("S2"))
                {
                    draft.getDraftedHitters().add(fantasyHitter);
                    draft.setDraftedPlayers();
                    int size = draft.getDraftedPlayers().size();
                    fantasyHitter.setPickNumber(size);
                }
                else
                {
                    draft.getDraftedHitters().remove(fantasyHitter);
                    draft.setDraftedPlayers();
                    handleRemoveDraftedPlayer(draft);
                }
                //OTHER VARIABLES THAT MAY HAVE NOT BEEN SET BECAUSE IT'S A NEW PLAYER
                fantasyHitter.setYearOfBirth(player.getYearOfBirth());
                fantasyHitter.setNationOfBirth(player.getNationOfBirth());
                //ADDS PLAYER TO FANTASY TEAM
                if(epd.getFantasyTeam(fantasyHitter.getFantasyTeamName())!=null)
                    epd.getFantasyTeam(fantasyHitter.getFantasyTeamName()).getFantasyHitters().add(fantasyHitter);
                else
                {
                    gui.showErrorDialog("You did not select a team. No player was edited.");
                    return;
                }
                FantasyTeam editedFT = epd.getFantasyTeam(fantasyHitter.getFantasyTeamName());
                editedFT.setCurrentSalary(editedFT.getCurrentSalary()-epd.getSalary());
                editedFT.setPlayersNeeded(editedFT.getPlayersNeeded()-1);
                int index = 0;
                //ADDS PLAYER TO FANTASY TEAMS
                for(FantasyTeam ft: fantasyTeams)
                {
                    if(ft.getName().equals(fantasyHitter.getFantasyTeamName()))
                    {
                        fantasyTeams.set(index, editedFT);
                        break;
                    }
                    index++;
                }
            }
            //PLAYER IS A PITCHER
            else
            {
                //USE THIS TO REMOVE PLAYERS AFTER WE'RE DONE WITH THEM
                pitcher = (FantasyPitcher)player;
                //INITIALIZING THE VARIABLES FOR THE FANTASY PLAYER
                fantasyPitcher = new FantasyPitcher(pitcher, 0, pitcher.getFantasyTeamName(), 0);
                //REMOVE FROM PLAYERS, ADD TO AVAILABLE PLAYERS.
                if(epd.getSelectedFantasyTeamString().equals("<Available Players>"))
                {
                    FantasyTeam changePlayer = obtainFantasyTeam(fantasyTeams);
                    int index = obtainFantasyTeamIndex(fantasyTeams);
                    int currentSalary = changePlayer.getCurrentSalary()+pitcher.getSalary();
                    int playersNeeded =changePlayer.getPlayersNeeded()+1;
                    changePlayer.setCurrentSalary(currentSalary);
                    changePlayer.setPlayersNeeded(playersNeeded);
                    //REMOVES PLAYER AND PUTS IT IN AVAAILABLE PLAYERS AREA
                    changePlayer.getFantasyPitchers().remove(pitcher);
                    draft.getDraftedPitchers().remove(pitcher);
                    draft.setDraftedPlayers();
                    handleRemoveDraftedPlayer(draft);
                    fantasyPitcher.setYearOfBirth(player.getYearOfBirth());
                    fantasyPitcher.setNationOfBirth(player.getNationOfBirth());
                    draft.getMLBPitchers().add(fantasyPitcher);
                    fantasyTeams.set(index, changePlayer);
                    
                    //RETURNS, BECAUSE NOTHING ELSE REALLY MATTERS, YOU'RE REMOVING THE PLAYER
                    return;
                }
                //NAME IS THE SAME, WE DON'T CHANGE IT'S TEAM
                if(epd.getSelectedFantasyTeamString().equals(fantasyPitcher.getFantasyTeamName()))
                {
                    //BUT WE SHOULD REMOVE THE PLAYER AS TO AVOID DUPLICATES
                    FantasyTeam changePlayer = obtainFantasyTeam(fantasyTeams);
                    if(epd.getSelectedPosition()!=null)
                    {
                        if(epd.getFantasyTeam(fantasyPitcher.getFantasyTeamName()).isAvailable(epd.getSelectedPosition()))
                             fantasyPitcher.setRole(epd.getSelectedPosition());
                        else
                        {
                            gui.showErrorDialog("The position is not available. The player was not edited.");
                            return;
                        }
                    }
                    draft.getDraftedPitchers().remove(pitcher);
                    draft.setDraftedPlayers();
                    handleRemoveDraftedPlayer(draft);
                    int currentSalary = changePlayer.getCurrentSalary()+pitcher.getSalary();
                    int playersNeeded =changePlayer.getPlayersNeeded()+1;
                    changePlayer.setCurrentSalary(currentSalary);
                    changePlayer.setPlayersNeeded(playersNeeded);
                    int index = obtainFantasyTeamIndex(fantasyTeams);
                    changePlayer.getFantasyPitchers().remove(pitcher);
                    fantasyTeams.set(index, changePlayer);
                    //ADDING THE PLAYER HAPPENS LATER ON, SO DON'T WORRY ABOUT IT
                }
                //NAME IS SOMETHING ELSE, SO WE REMOVE THE PLAYER AND THEN PUT IT INTO THE OTHER TEAM.
                else
                {
                    FantasyTeam removePlayer = obtainFantasyTeam(fantasyTeams);
                    fantasyPitcher.setFantasyTeamName(epd.getSelectedFantasyTeamString());
                    if(epd.getSelectedPosition()!=null)
                    {
                        if(epd.getFantasyTeam(fantasyPitcher.getFantasyTeamName()).isAvailable(epd.getSelectedPosition()))
                             fantasyPitcher.setRole(epd.getSelectedPosition());
                        else
                        {
                            gui.showErrorDialog("The position is not available. The player was not edited.");
                            return;
                        }
                    }
                    int currentSalary = removePlayer.getCurrentSalary()+pitcher.getSalary();
                    int playersNeeded =removePlayer.getPlayersNeeded()+1;
                    draft.getDraftedPitchers().remove(pitcher);
                    draft.setDraftedPlayers();
                    handleRemoveDraftedPlayer(draft);
                    removePlayer.setCurrentSalary(currentSalary);
                    removePlayer.setPlayersNeeded(playersNeeded);
                    int index = obtainFantasyTeamIndex(fantasyTeams);
                    removePlayer.getFantasyPitchers().remove(pitcher);
                    fantasyTeams.set(index, removePlayer);
                    //ADDING THE PLAYER HAPPENS LATER ON, SO DON'T WORRY ABOUT IT
                }
                if(epd.getSalary()!=0)
                    fantasyPitcher.setSalary(epd.getSalary());
                if(epd.getContract()!=null)
                {
                    int contract = 0;
                    if(epd.getContract().equals("S1"))
                        contract = 1;
                    if(epd.getContract().equals("S2"))
                        contract = 2;
                    fantasyPitcher.getContractString(contract);
                }
                else
                {
                    gui.showErrorDialog("You did not select a contract. No player was added.");
                    return;
                }
                if(fantasyPitcher.getContractString(fantasyPitcher.getContractInt()).equals("S2"))
                {
                    draft.getDraftedPitchers().add(fantasyPitcher);
                    draft.setDraftedPlayers();
                    int size = draft.getDraftedPlayers().size();
                    fantasyPitcher.setPickNumber(size);
                }
                else
                {
                    draft.getDraftedPitchers().remove(fantasyPitcher);
                    draft.setDraftedPlayers();
                    handleRemoveDraftedPlayer(draft);
                }
                if(epd.getSelectedPosition()!=null)
                    fantasyPitcher.setRole(epd.getSelectedPosition());
                fantasyPitcher.setYearOfBirth(player.getYearOfBirth());
                fantasyPitcher.setNationOfBirth(player.getNationOfBirth());
                //ADDS PLAYER TO FANTASY TEAM
                epd.getFantasyTeam(fantasyPitcher.getFantasyTeamName()).getFantasyPitchers().add(fantasyPitcher);
                FantasyTeam editedFT = epd.getFantasyTeam(fantasyPitcher.getFantasyTeamName());
                editedFT.setCurrentSalary(editedFT.getCurrentSalary()-epd.getSalary());
                editedFT.setPlayersNeeded(editedFT.getPlayersNeeded()-1);
                int index = 0;
                for(FantasyTeam ft: fantasyTeams)
                {
                    if(ft.getName().equals(fantasyPitcher.getFantasyTeamName()))
                    {
                        fantasyTeams.set(index, editedFT);
                        break;
                    }
                    index++;
                }
            }
        }
        else{
            //USER PRESSED CANCEL, DO NOTHING
        }
    }
    private FantasyTeam obtainFantasyTeam(ObservableList<FantasyTeam> fantasyTeams)
    {
        FantasyTeam changePlayer = null;
        if(fantasyHitter !=null)
        {
            for(FantasyTeam ft: fantasyTeams)
            {
                if(ft.getName().equals(fantasyHitter.getFantasyTeamName()))
                {
                    changePlayer = ft;
                    break;
                }
            }
        }
        else
        {
            for(FantasyTeam ft: fantasyTeams)
            {
                if(ft.getName().equals(fantasyPitcher.getFantasyTeamName()))
                {
                    changePlayer = ft;
                    break;
                }
            }
        }
        return changePlayer;
    }
    private int obtainFantasyTeamIndex(ObservableList<FantasyTeam> fantasyTeams)
    {
        int index = 0;
        if(fantasyHitter !=null)
        {
            for(FantasyTeam ft: fantasyTeams)
            {
                if(ft.getName().equals(fantasyHitter.getFantasyTeamName()))
                    break;
                index++;
            }
        }
        else
        {
            for(FantasyTeam ft: fantasyTeams)
            {
                if(ft.getName().equals(fantasyPitcher.getFantasyTeamName()))
                    break;
                index++;
            }
        }
        return index;
    }
    private void handleRemoveDraftedPlayer(WolfieballDraft d)
    {
        ObservableList<MLBPlayer> players = d.getDraftedPlayers();
        for(int i = 0; i<players.size()-1; i++)
        {
            players.set(i, players.get(i+1));
            players.get(i).setPickNumber(i+1);
        }
        d.setDraftedPlayers(players);
    }
}
