
package wolfieball.gui;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wolfieball.data.FantasyHitter;
import wolfieball.data.FantasyPitcher;
import wolfieball.data.FantasyTeam;
import wolfieball.data.WolfieballDraft;

/**
 *
 * @author Tristen
 */
public class FantasyTeamDialog extends Stage{
    //OBJECT BEHIND UI
    FantasyTeam team;
    
    VBox bodyPane;
    Scene bodyScene;
    
    Label fantasyTeamDetailsLabel;
    Label fantasyTeamNameLabel;
    Label fantasyTeamOwnerLabel;
    
    TextField fantasyNameTextField;
    TextField fantasyOwnerTextField;
    
    FlowPane fantasyTeamNameToolbar;
    FlowPane fantasyOwnerToolbar;
    FlowPane completeCancelToolbar;
    
    //BUTTONS TO COMPLETE/CANCEL
    Button completeButton;
    Button cancelButton;
    
    String selection;
    
    public FantasyTeamDialog(Stage owner, WolfieballDraft draft)
    {
        // MAKE IT MODAL
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);
        bodyPane = new VBox();
        bodyPane.setPrefWidth(400);
        bodyPane.setPrefHeight(200);
        bodyScene = new Scene(bodyPane);
        fantasyTeamDetailsLabel = new Label("Fantasy Team Details");
        fantasyTeamDetailsLabel.setFont(new Font("Arial", 30));
        fantasyTeamNameLabel = new Label("Name: ");
        fantasyTeamOwnerLabel = new Label("Owner: ");
        
        this.setTitle("Add Fantasy Team");
        fantasyNameTextField = new TextField();
        fantasyOwnerTextField = new TextField();
        
        fantasyTeamNameToolbar = new FlowPane();
        fantasyOwnerToolbar = new FlowPane();
        fantasyTeamNameToolbar.getChildren().addAll(fantasyTeamNameLabel, 
                                                    fantasyNameTextField);
        fantasyOwnerToolbar.getChildren().addAll(fantasyTeamOwnerLabel, 
                                                 fantasyOwnerTextField);
        
        completeButton = new Button("Complete");
        cancelButton = new Button("Cancel");
        completeCancelToolbar = new FlowPane();
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            this.selection = sourceButton.getText();
            this.hide();
        };
        fantasyNameTextField.clear();
        fantasyOwnerTextField.clear();
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);
        completeCancelToolbar.getChildren().addAll(completeButton,
                                                   cancelButton);
        bodyPane.getChildren().addAll(fantasyTeamDetailsLabel,
                                      fantasyTeamNameToolbar,
                                      fantasyOwnerToolbar,
                                      completeCancelToolbar);
        this.setScene(bodyScene);
    }
    public boolean wasCompleteSelected() {
        return selection.equals("Complete");
    }
    public FantasyTeam showAddFantasyTeamDialog(ObservableList<FantasyTeam> teams) {
        this.showAndWait();
        if(fantasyNameTextField.getText().equals("")||fantasyOwnerTextField.getText().equals(""))
            return null;
        String fantasyTeamName = fantasyNameTextField.getText();
        String fantasyOwnerName = fantasyOwnerTextField.getText();
        ArrayList<FantasyHitter> hitters = new ArrayList<FantasyHitter>();
        ArrayList<FantasyPitcher> pitchers = new ArrayList<FantasyPitcher>();
        team = new FantasyTeam(fantasyTeamName, fantasyOwnerName, pitchers, hitters);
        return team;
    }
    public void showEditFantasyTeamDialog(FantasyTeam teamToEdit)
    {
        setTitle("Edit Fantasy Team");
        team = new FantasyTeam(teamToEdit.getName(), teamToEdit.getOwnerName(), teamToEdit.getFantasyPitchers(), teamToEdit.getFantasyHitters());
        ArrayList<FantasyHitter> hitters =teamToEdit.getFantasyHitters();
        ArrayList<FantasyPitcher> pitchers =teamToEdit.getFantasyPitchers();
        loadGUIData();
        this.showAndWait();
        if(fantasyNameTextField.getText().equals("")||fantasyOwnerTextField.getText().equals(""))
            return;
        teamToEdit.setName(fantasyNameTextField.getText());
        teamToEdit.setOwnerName(fantasyOwnerTextField.getText());
        team = new FantasyTeam(teamToEdit.getName(), teamToEdit.getOwnerName(), pitchers, hitters);
    }
    private void loadGUIData()
    {
        fantasyNameTextField.setText(team.getName());
        fantasyOwnerTextField.setText(team.getOwnerName());
    }
    public FantasyTeam getFantasyTeam()
    {
        return team;
    }
}
