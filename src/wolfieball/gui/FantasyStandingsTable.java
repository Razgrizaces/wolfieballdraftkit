
package wolfieball.gui;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.Callback;
import wolfieball.data.FantasyHitter;
import wolfieball.data.FantasyPitcher;
import wolfieball.data.FantasyTeam;
import wolfieball.file.JsonDraftFileManager;
import wolfieball.mlb.MLBHitter;
import wolfieball.mlb.MLBPitcher;
import wolfieball.mlb.MLBPlayer;

/**
 *
 * @author Tristen
 */
public class FantasyStandingsTable extends TableView{
    private TableColumn teamNameColumn;
    private TableColumn playersNeededColumn;
    private TableColumn moneyLeftColumn;
    private TableColumn moneyPerPlayerColumn;
    private TableColumn teamRunsColumn;
    private TableColumn teamHomeRunsColumn;
    private TableColumn teamRBIColumn;
    private TableColumn teamStolenBasesColumn;
    private TableColumn teamBattingAverageColumn;
    private TableColumn teamWinsColumn;
    private TableColumn teamSavesColumn;
    private TableColumn teamStrikeoutsColumn;
    private TableColumn teamERAColumn;
    private TableColumn teamWHIPColumn;
    private TableColumn teamTotalPointsColumn;
    private ObservableList<FantasyTeam> fantasyTeams;
    private JsonDraftFileManager fileManager = new JsonDraftFileManager();
    
    public FantasyStandingsTable()
    {
        super();
    }
    public void initialize()
    {
        //ADDING COLUMNS
        teamNameColumn = new TableColumn("Team Name");
        teamNameColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, String>("name")); 
        playersNeededColumn = new TableColumn("Players Needed");
        playersNeededColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, String>("playersNeeded")); 
        moneyLeftColumn = new TableColumn("$ Left");
        moneyLeftColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, String>("currentSalary")); 
        moneyPerPlayerColumn = new TableColumn("$ PP");
        moneyPerPlayerColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, String>("salaryPerPlayer")); 
        teamRunsColumn = new TableColumn("R");
        teamRunsColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, String>("teamRuns"));
        teamHomeRunsColumn = new TableColumn("HR");
        teamHomeRunsColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, String>("teamHomeRuns"));
        teamRBIColumn = new TableColumn("RBI");
        teamRBIColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, String>("teamRunsBattedIn"));        
        teamStolenBasesColumn = new TableColumn("SB");
        teamStolenBasesColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, String>("teamStolenBases"));   
        teamBattingAverageColumn = new TableColumn("BA");
        teamBattingAverageColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, String>("teamBattingAverage")); 
        teamWinsColumn = new TableColumn("W");
        teamWinsColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, String>("teamWins")); 
        teamSavesColumn = new TableColumn("SV");
        teamSavesColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, String>("teamSaves")); 
        teamStrikeoutsColumn = new TableColumn("K");
        teamStrikeoutsColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, String>("teamStrikeouts")); 
        teamERAColumn = new TableColumn("ERA");
        teamERAColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, String>("teamEarnedRunAverage")); 
        teamWHIPColumn = new TableColumn("WHIP");
        teamWHIPColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, String>("teamWalksHitsInningsPitched")); 
        teamTotalPointsColumn = new TableColumn("Total Points");
        teamTotalPointsColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, String>("teamPoints")); 
        //ADDING COLUMNS TO TABLE
        this.getColumns().addAll(teamNameColumn,
                                 playersNeededColumn,
                                 moneyLeftColumn,
                                 moneyPerPlayerColumn,
                                 teamRunsColumn,
                                 teamHomeRunsColumn,
                                 teamRBIColumn,
                                 teamStolenBasesColumn,
                                 teamBattingAverageColumn,
                                 teamWinsColumn,
                                 teamSavesColumn,
                                 teamStrikeoutsColumn,
                                 teamERAColumn,
                                 teamWHIPColumn,
                                 teamTotalPointsColumn);
        this.setEditable(true);
        this.setMinSize(1200, 600);
    }
    public void loadTeams()
    {
        this.setItems(fantasyTeams);
    }
    public void setTeams(ObservableList<FantasyTeam> ft)
    {
        if(fantasyTeams == null)
            fantasyTeams = FXCollections.observableArrayList();
        fantasyTeams.addAll(ft);
    }
}
