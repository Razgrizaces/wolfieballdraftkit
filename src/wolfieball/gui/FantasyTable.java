package wolfieball.gui;

import java.util.ArrayList;
import java.util.Comparator;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.Callback;
import wolfieball.data.FantasyHitter;
import wolfieball.data.FantasyPitcher;
import wolfieball.file.JsonDraftFileManager;
import wolfieball.mlb.MLBHitter;
import wolfieball.mlb.MLBPitcher;
import wolfieball.mlb.MLBPlayer;

public class FantasyTable extends TableView{
    private TableColumn firstNameColumn;
    private TableColumn lastNameColumn;
    private TableColumn proTeamColumn;
    private TableColumn positionsColumn;
    private TableColumn runsWinsColumn;
    private TableColumn homeRunsSavesColumn;
    private TableColumn RBIStrikeoutsColumn;
    private TableColumn stolenBasesERAColumn;
    private TableColumn battingAverageWHIPColumn;
    private TableColumn estimatedValueColumn;
    private TableColumn contractColumn;
    private TableColumn salaryColumn;
    private TableColumn positionColumn;
    private ObservableList<FantasyHitter> fantasyHitters;
    private ObservableList<FantasyPitcher> fantasyPitchers;
    private ObservableList<MLBPlayer> mlbPlayers;
    private JsonDraftFileManager fileManager = new JsonDraftFileManager();
    
    public FantasyTable()
    {
        super();
    }
    public void initialize()
    {
        //ADDING COLUMNS
        positionColumn = new TableColumn("Position");
        positionColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<MLBPlayer, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<MLBPlayer, String> param) {
                boolean isHitter = param.getValue().getIsHitter();
                if(isHitter)
                {
                    FantasyHitter h = (FantasyHitter)(param.getValue());
                    return new ReadOnlyObjectWrapper(h.getCurrentPosition());
                }
                else
                {
                    FantasyPitcher p = (FantasyPitcher)param.getValue();
                    return new ReadOnlyObjectWrapper(p.getRole());
                }
            }
        });
        positionColumn.setSortable(true);
        firstNameColumn = new TableColumn("First");
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<MLBPlayer, String>("firstName"));
        firstNameColumn.setSortable(true);
        lastNameColumn = new TableColumn("Last");
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<MLBPlayer, String>("lastName"));
        firstNameColumn.setSortable(true);
        proTeamColumn = new TableColumn("Pro Team");
        proTeamColumn.setCellValueFactory(new PropertyValueFactory<MLBPlayer, String>("team"));
        positionsColumn = new TableColumn("Positions");
        positionsColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<MLBPlayer, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<MLBPlayer, String> param) {
                boolean isHitter = param.getValue().getIsHitter();
                if(isHitter)
                {
                    MLBHitter h = (MLBHitter)(param.getValue());
                    return new ReadOnlyObjectWrapper(h.getQualifyingPositionString());
                }
                else
                {
                    MLBPitcher p = (MLBPitcher)param.getValue();
                    return new ReadOnlyObjectWrapper(p.getRole());
                }
            }
        });
        runsWinsColumn = new TableColumn("R/W");
        runsWinsColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<MLBPlayer, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<MLBPlayer, String> param) {
                boolean isHitter = param.getValue().getIsHitter();
                if(isHitter)
                {
                    MLBHitter h = (MLBHitter)(param.getValue());
                    return new ReadOnlyObjectWrapper(h.getRuns());
                }
                else
                {
                    MLBPitcher p = (MLBPitcher)param.getValue();
                    return new ReadOnlyObjectWrapper(p.getWins());
                }
            }
        });
        homeRunsSavesColumn = new TableColumn("HR/SV");
        homeRunsSavesColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<MLBPlayer, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<MLBPlayer, String> param) {
                boolean isHitter = param.getValue().getIsHitter();
                
                if(isHitter)
                {
                    MLBHitter h = (MLBHitter)(param.getValue());
                    return new ReadOnlyObjectWrapper(h.getHomeRuns());
                }
                else
                {
                    MLBPitcher p = (MLBPitcher)param.getValue();
                    return new ReadOnlyObjectWrapper(p.getSaves());
                }
            }
        });
        RBIStrikeoutsColumn = new TableColumn("RBI/K");
        RBIStrikeoutsColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<MLBPlayer, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<MLBPlayer, String> param) {
                boolean isHitter = param.getValue().getIsHitter();
                
                if(isHitter)
                {
                    MLBHitter h = (MLBHitter)(param.getValue());
                    return new ReadOnlyObjectWrapper(h.getRunsBattedIn());
                }
                else
                {
                    MLBPitcher p = (MLBPitcher)param.getValue();
                    return new ReadOnlyObjectWrapper(p.getStrikeouts());
                }
            }
        });        
        stolenBasesERAColumn = new TableColumn("SB/ERA");
        stolenBasesERAColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<MLBPlayer, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<MLBPlayer, String> param) {
                boolean isHitter = param.getValue().getIsHitter();
                
                if(isHitter)
                {
                    MLBHitter h = (MLBHitter)(param.getValue());
                    return new ReadOnlyObjectWrapper(h.getStolenBases());
                }
                else
                {
                    MLBPitcher p = (MLBPitcher)param.getValue();
                    return new ReadOnlyObjectWrapper(p.getERA());
                }
            }
        });   
        battingAverageWHIPColumn = new TableColumn("BA/WHIP");
        battingAverageWHIPColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<MLBPlayer, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<MLBPlayer, String> param) {
                boolean isHitter = param.getValue().getIsHitter();
                
                if(isHitter)
                {
                    MLBHitter h = (MLBHitter)(param.getValue());
                    return new ReadOnlyObjectWrapper(h.getBattingAverage());
                }
                else
                {
                    MLBPitcher p = (MLBPitcher)param.getValue();
                    return new ReadOnlyObjectWrapper(p.getWHIP());
                }
            }
        }); 
        estimatedValueColumn = new TableColumn("Estimated Value");
        estimatedValueColumn.setCellValueFactory(new PropertyValueFactory<MLBPlayer, String>("estimatedValue"));
        contractColumn = new TableColumn("Contract");
        contractColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<MLBPlayer, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<MLBPlayer, String> param) {
                boolean isHitter = param.getValue().getIsHitter();
                if(isHitter)
                {
                    FantasyHitter h = (FantasyHitter)(param.getValue());
                    return new ReadOnlyObjectWrapper(h.getContractString(h.getContractInt()));
                }
                else
                {
                    FantasyPitcher p = (FantasyPitcher)param.getValue();
                    return new ReadOnlyObjectWrapper(p.getContractString(p.getContractInt()));
                }
            }
        });
        salaryColumn = new TableColumn("Salary");
        salaryColumn.setCellValueFactory(new PropertyValueFactory<MLBPlayer, String>("Salary"));
        //ADDING COLUMNS TO TABLE
        this.getColumns().addAll(positionColumn,
                                 firstNameColumn,
                                 lastNameColumn,
                                 proTeamColumn,
                                 positionsColumn,
                                 runsWinsColumn,
                                 homeRunsSavesColumn,
                                 RBIStrikeoutsColumn,
                                 stolenBasesERAColumn,
                                 battingAverageWHIPColumn,
                                 estimatedValueColumn,
                                 contractColumn,
                                 salaryColumn);
        this.setEditable(true);
        this.setMinSize(1200, 600);
    }
    public void loadPlayers()
    {
        mlbPlayers = FXCollections.observableArrayList();
        fantasyHitters.sort(new Comparator<FantasyHitter>() {
            @Override
            public int compare(FantasyHitter o1, FantasyHitter o2) {
                String o1QP = o1.getCurrentPosition();
                String o2QP = o2.getCurrentPosition();
                String catcher = "C";
                String firstBase = "1B";
                String cornerInf = "CI";
                String thirdBase = "3B";
                String secondBase = "2B";
                String middleInf = "MI";
                String shortStop = "SS";
                String outfield = "OF";
                String utility = "U";
                if(o1QP.equals(o2QP)){
                    return 0;
                }
                if(o1QP.equals(catcher))
                    return -1;
                if(o2QP.equals(catcher))
                    return 1;
                if(o1QP.equals(firstBase))
                    return -1;
                if(o1QP.equals(cornerInf))
                {
                    if(o2QP.equals(firstBase))
                        return 1;
                    else
                        return -1;
                }
                if(o1QP.equals(thirdBase))
                {
                    if(o2QP.equals(firstBase)
                            ||o2QP.equals(cornerInf))
                        return 1;
                    else
                        return -1;
                }
                if(o1QP.equals(secondBase))
                {
                    if(o2QP.equals(firstBase)
                            ||o2QP.equals(cornerInf)
                            ||o2QP.equals(thirdBase))
                        return 1;
                    else
                        return -1;
                }
                if(o1QP.equals(middleInf))
                {
                    if(o2QP.equals(firstBase)
                            ||o2QP.equals(cornerInf)
                            ||o2QP.equals(thirdBase)
                            ||o2QP.equals(secondBase))
                        return 1;
                    else
                        return -1;
                }
                if(o1QP.equals(shortStop))
                {
                    if(o2QP.equals(firstBase)
                            ||o2QP.equals(cornerInf)
                            ||o2QP.equals(thirdBase)
                            ||o2QP.equals(secondBase)
                            ||o2QP.equals(middleInf))
                        return 1;
                    else
                        return -1;
                }
                if(o1QP.equals(outfield))
                {
                    if(o2QP.equals(firstBase)
                            ||o2QP.equals(cornerInf)
                            ||o2QP.equals(thirdBase)
                            ||o2QP.equals(secondBase)
                            ||o2QP.equals(middleInf)
                            ||o2QP.equals(shortStop))
                        return 1;
                    else
                        return -1;
                }
                if(o1QP.equals(utility))
                        return 1;
                return -1;
            }
        });
        mlbPlayers.addAll(fantasyHitters);
        mlbPlayers.addAll(fantasyPitchers);
    }
    public void loadTaxiPlayers()
    {
        if(fantasyHitters == null)
            fantasyHitters = FXCollections.observableArrayList();
        mlbPlayers.setAll(fantasyHitters);
    }
    public void showPlayers()
    {
        this.setItems(mlbPlayers);
    }
    public void clearPlayers()
    {
        mlbPlayers.clear();
    }
    public ObservableList<MLBPlayer> getAllMLBPlayers()
    {
        return mlbPlayers;
    }
    public ObservableList<FantasyHitter> getFantasyHitters()
    {
        return fantasyHitters;
    }
    public ObservableList<FantasyPitcher> getFantasyPitchers()
    {
        return fantasyPitchers;
    }
    public void setMLBPlayers(ObservableList<MLBPlayer> p)
    {
        if(mlbPlayers == null)
            mlbPlayers = FXCollections.observableArrayList();
        else
            mlbPlayers.clear();
        mlbPlayers.addAll(p);
    }
    public void setMLBPlayers(ObservableList<MLBHitter> h, ObservableList<MLBPitcher> p)
    {
        if(mlbPlayers== null)
            mlbPlayers = FXCollections.observableArrayList();
        else
            mlbPlayers.clear();
        mlbPlayers.addAll(h);
        mlbPlayers.addAll(p);
    }
    public void setFantasyHitters(ObservableList<FantasyHitter> h)
    {
        if(fantasyHitters == null)
            fantasyHitters = FXCollections.observableArrayList();
        else
            fantasyHitters.clear();
        fantasyHitters.addAll(h);
    }
    public void setFantasyPitchers(ObservableList<FantasyPitcher> p )
    {
        if(fantasyPitchers == null)
            fantasyPitchers = FXCollections.observableArrayList();
        else
            fantasyPitchers.clear();
        fantasyPitchers.addAll(p);
    }
}
