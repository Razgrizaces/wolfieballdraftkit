
package wolfieball.gui;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wolfieball.data.FantasyHitter;
import wolfieball.data.FantasyPitcher;
import wolfieball.data.FantasyTeam;
import wolfieball.data.WolfieballDraft;
import wolfieball.mlb.MLBHitter;
import wolfieball.mlb.MLBPitcher;
import wolfieball.mlb.MLBPlayer;

/**
 *
 * @author Tristen
 */
public class EditPlayerDialog extends Stage{
    
    //OBJECT OF UI
    MLBPlayer player;
    FantasyHitter editPlayerHitter;
    FantasyPitcher editPlayerPitcher;
    
    VBox bodyPane;
    GridPane playerDetailsPane;
    Scene bodyScene;
    VBox playerDetailsToolbar;
    
    Label playerDetailsLabel;
    Label fantasyTeamLabel;
    Label selectPositionLabel;
    Label selectContractLabel;
    Label selectSalaryLabel;
    Label eligiblePositionLabel;
    Label playerNameLabel;
    
    TextField salaryTextField;
    
    ComboBox fantasyTeamComboBox;
    ComboBox selectPositionComboBox;
    ComboBox selectContractComboBox;
    
    VBox editPlayerControls;
    FlowPane fantasyTeamToolbar;
    FlowPane positionToolbar;
    FlowPane contractToolbar;
    FlowPane salaryToolbar;
    FlowPane completeCancelToolbar;
    
    //BUTTONS TO COMPLETE/CANCEL
    Button completeButton;
    Button cancelButton;
    
    //TO GET THE MLB PLAYER LIST
    Wolfieball_GUI gui;
    
    //IMAGES FOR PLAYER
    ImageView playerImage;
    ImageView playerNationImage;
    
    //STRINGS TO IMPORT THE PATHS FOR IMAGES
    String draftPathNationOfBirth = "file:C:\\Users\\Tristen\\Documents\\NetBeansProjects\\Wolfieball\\data\\wolfieball_images\\flags\\";
    String draftPathPicture = "file:C:\\Users\\Tristen\\Documents\\NetBeansProjects\\Wolfieball\\data\\wolfieball_images\\players\\";
    String draftPathMissingPlayer = "AAA_PhotoMissing";
    String draftPathJPG = ".jpg";
    String draftPathPNG = ".png";
    
    //THE SELECTED OPTION
    String selection;
    MessageDialog errorDialog;
    
    //WE'LL NEED TO LOAD THE PLAYER
    ObservableList<MLBPlayer> players;
    WolfieballDraft draft;
    ObservableList<FantasyTeam> fantasyTeams;
    
    public EditPlayerDialog(Stage owner, WolfieballDraft initDraft)
    {
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);
        gui = new Wolfieball_GUI(owner);
        bodyPane = new VBox();
        bodyPane.setPrefWidth(500);
        bodyPane.setPrefHeight(500);
        bodyScene = new Scene(bodyPane);
        playerDetailsPane = new GridPane();
        errorDialog = new MessageDialog(owner, "OK");
        draft = initDraft;
        playerDetailsLabel = new Label("Player Details");
        playerDetailsLabel.setFont(new Font("Arial", 30));
        playerDetailsPane.add(playerDetailsLabel, 0, 0);
        fantasyTeamLabel = new Label("Fantasy Team");
        selectPositionLabel = new Label("Position");
        selectContractLabel = new Label("Contract");
        selectSalaryLabel = new Label("Salary ($)");
        eligiblePositionLabel = new Label();
        playerNameLabel = new Label();
        
        fantasyTeamToolbar = new FlowPane();
        positionToolbar = new FlowPane();
        contractToolbar = new FlowPane();
        salaryToolbar = new FlowPane();
        completeCancelToolbar = new FlowPane();
        playerImage = new ImageView();
        playerNationImage = new ImageView();
        editPlayerControls = new VBox();
        initFantasyTeamComboBox();
        selectPositionComboBox = new ComboBox();
        selectContractComboBox = new ComboBox();
        initContractComboBox();
        salaryTextField = new TextField();
        completeButton = new Button("Complete");
        cancelButton = new Button("Cancel");
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            this.selection = sourceButton.getText();
            this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);
        playerDetailsPane.add(playerNameLabel, 1, 2);
        playerDetailsPane.add(eligiblePositionLabel, 1, 3);
        completeCancelToolbar.getChildren().addAll(completeButton, 
                                                cancelButton);
        fantasyTeamToolbar.getChildren().addAll(fantasyTeamLabel,
                                                fantasyTeamComboBox);
        positionToolbar.getChildren().addAll(selectPositionLabel,
                                             selectPositionComboBox);
        contractToolbar.getChildren().addAll(selectContractLabel,
                                             selectContractComboBox);
        salaryToolbar.getChildren().addAll(selectSalaryLabel,
                                        salaryTextField);
        editPlayerControls.getChildren().addAll(fantasyTeamToolbar,
                                            positionToolbar,
                                            contractToolbar,
                                            salaryToolbar,
                                            completeCancelToolbar);
        bodyPane.getChildren().addAll(playerDetailsPane,
                                      editPlayerControls);
        this.setScene(bodyScene);
    }
    private void initFantasyTeamComboBox() {
        //GETS FANTASY TEAMS AND LOADS THEM
        fantasyTeams = draft.getFantasyTeams();
        ObservableList<String> obsFTString = FXCollections.observableArrayList();
        for(FantasyTeam ft: fantasyTeams)
            obsFTString.add(ft.getName());
        fantasyTeamComboBox = new ComboBox(obsFTString);
        if(obsFTString==null)
            errorDialog.show("There are no Fantasy Teams. Please input a fantasy team first.");
    }

    private void initPositionComboBox() {
        ObservableList<String> positions = FXCollections.observableArrayList();
        positionToolbar.getChildren().clear();
        if(player.getIsHitter()==false)
        {
            positions.add("P");
            selectPositionComboBox = new ComboBox(positions);
        }
        else
        {
            MLBHitter hitter = (MLBHitter)player;
            positions.addAll(hitter.getQualifyingPositions());
            selectPositionComboBox = new ComboBox(positions);
        }
        positionToolbar.getChildren().addAll(selectPositionLabel,
                                             selectPositionComboBox);
    }
    private void initContractComboBox() {
        ObservableList<String> contracts = FXCollections.observableArrayList("S2", "S1", "X");
        selectContractComboBox = new ComboBox(contracts);
    }
    private void loadImages()
    {
        String playerFirstName = player.getFirstName();
        String playerLastName = player.getLastName();
        playerImage = new ImageView(draftPathPicture + playerLastName+playerFirstName + draftPathJPG);
        if(playerImage.getImage().getHeight()==0)
            playerImage = new ImageView(draftPathPicture + draftPathMissingPlayer+ draftPathJPG);
        playerNationImage = new ImageView(draftPathNationOfBirth + player.getNationOfBirth()+ draftPathPNG);
        playerDetailsPane.add(playerImage, 0, 2);
        playerDetailsPane.add(playerNationImage, 1, 1);
    }
    public void showEditPlayerDialog(MLBPlayer playerToEdit)
    {
        player = new MLBPlayer();
        MLBHitter h;
        MLBPitcher p;
        String qualifyingPosition = "";
        if(playerToEdit.getIsHitter())
        {
            h = (MLBHitter)playerToEdit;
            qualifyingPosition = h.getQualifyingPosition();
            player = (MLBPlayer)h;
        }
        else
        {
            p = (MLBPitcher)playerToEdit;
            qualifyingPosition = p.getRole();
            player = (MLBPlayer)p;
        }
        player.setNationOfBirth(playerToEdit.getNationOfBirth());
        player.setName(playerToEdit.getFirstName(), playerToEdit.getLastName());
        loadImages();
        initPositionComboBox();
        String name = player.getFirstName() + " " + player.getLastName();
        playerNameLabel.setText(name);
        playerNameLabel.setFont(new Font("Arial", 20));
        eligiblePositionLabel.setText(qualifyingPosition);
        this.showAndWait();
    }
    //HERE WE CLEAR THE SELECTIONS
    public void clear()
    {
        initContractComboBox();
        initPositionComboBox();
        initFantasyTeamComboBox();
        salaryTextField.setText("");
    }
    public boolean wasCompleteSelected() {
        return selection.equals("Complete");
    }
    public MLBPlayer getPlayer()
    {
        return player;
    }
    public String getContract()
    {
        return (String)selectContractComboBox.getSelectionModel().getSelectedItem();
    }
    public String getSelectedPosition()
    {
        return (String)selectPositionComboBox.getSelectionModel().getSelectedItem();
    }
    public String getSelectedFantasyTeamString()
    {
        return (String)fantasyTeamComboBox.getSelectionModel().getSelectedItem();
    }
    public FantasyTeam getFantasyTeam(String ftn)
    {
        if(fantasyTeams==null)
            return null;
        else
        {
            for(FantasyTeam t: fantasyTeams)
                if(t.getName().equals(ftn))
                    return t;
            return null;
        }
    }
    public int getSalary()
    {
        try{
            return Integer.parseInt(salaryTextField.getText());
        }
        catch(Exception e)
        {
            errorDialog.show("Either empty string or characters in text box. Please try again.");
            return 0;
        }
    }
    public void showErrorDialog(String message)
    {
        errorDialog.show(message);
    }
}
