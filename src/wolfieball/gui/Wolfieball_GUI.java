/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wolfieball.gui;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.Window;
import static wolfieball.WolfieballStartupConstants.CLOSE_BUTTON_LABEL;
import static wolfieball.WolfieballStartupConstants.PATH_CSS;
import wolfieball.controller.AvailablePlayersController;
import wolfieball.controller.FantasyTeamsController;
import wolfieball.controller.FileController;
import wolfieball.data.DraftDataView;
import wolfieball.data.WolfieballDraft;
import wolfieball.data.DraftDataManager;
import wolfieball.data.FantasyHitter;
import wolfieball.data.FantasyPitcher;
import wolfieball.data.FantasyTeam;
import wolfieball.file.DraftExporter;
import wolfieball.file.DraftFileManager;
import wolfieball.mlb.MLBHitter;
import wolfieball.mlb.MLBPitcher;
import wolfieball.mlb.MLBPlayer;

/**
 *
 * @author Tristen
 */
public class Wolfieball_GUI implements DraftDataView {

    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "Wolfieball_style.css";
    
    //PLAYERS SCREEN VARIABLES
    
    //ADD/SUBTRACT PLAYERS FOR PLAYERS SCREEN
    FlowPane playerControls;
    Button addPlayerButton;
    Button removePlayerButton;
    
    //SEARCH FUNCTION FOR PLAYERS SCREEN
    HBox playerFilters;
    Label searchLabel;
    TextField searchField;

    //RADIO BUTTON FIELD FOR PLAYERS SCREEN
    final ToggleGroup group;
    RadioButton allPlayers;
    RadioButton catchers;
    RadioButton firstBasemen;
    RadioButton cornerInfielders;
    RadioButton thirdBasemen;
    RadioButton secondBasemen;
    RadioButton middleInfielders;
    RadioButton shortstop;
    RadioButton outfielders;
    RadioButton utilityPlayers;
    RadioButton pitchers;

    //WE'LL NEED A TABLE FOR OUR PLAYERS
    PlayersTable playersTable;
    
    //AND A CONTROLLER FOR ANY OPEN DIALOGS
    AvailablePlayersController apc;
    
    //FANTASY SCREEN VARIABLES
    
    //ADD/SUBTRACT PLAYERS FOR FANTASY SCREEN
    FlowPane fantasyControls;
    Button addFantasyTeamButton;
    Button removeFantasyTeamButton;
    Button editFantasyTeamButton;
    
    //DRAFT TOOLBAR 
    FlowPane fantasyDraftNameToolbar;
    Label draftNameLabel;
    TextField draftNameField;
    
    //SELECT FANTASY TEAM
    Label selectFantasyTeamLabel;
    ComboBox fantasyTeamComboBox;
    
    //STARTING LINEUP TABLE
    Label startingLineupLabel;
    Label taxiSquadLabel;
    FantasyTable startingLineupTable;
    FantasyTable taxiSquadTable;
    
    //FANTASY TEAMS DIALOG
    FantasyTeamDialog fantasyTeamDialog;
    YesNoCancelDialog removeFantasyTeamDialog;
    EditPlayerDialog editFantasyPlayerDialog;
    YesNoCancelDialog removeFantasyPlayerDialog;
    
    VBox startingLineupVBox;
    VBox taxiSquadVBox;
    
    //THE LIST OF FANTASY TEAMS
    ObservableList<FantasyTeam> fantasyTeams;
    
    //LIST OF PLAYERS FOR FANTASY TEAMS
    ObservableList<FantasyHitter> fantasyHitters;
    ObservableList<FantasyPitcher> fantasyPitchers;
    
    //CONTROLLER FOR FANTASY TEAMS
    FantasyTeamsController fantasyController;
    EditFantasyPlayerDialog efpd;
    
    //FANTASY STANDINGS SCREEN VARIABLES
    FantasyStandingsTable standingsTable;
    Label fantasyStandingsLabel;
    
    //DRAFT SUMMARY SCREEN VARIABLES
    DraftTable draftTable;
    FlowPane draftToolbar;
    Button selectPlayerButton;
    Button startAutoDraft;
    Button pauseAutoDraft;
    ObservableList<MLBPlayer> draftedPlayers;
    
    //AUTOMATING DRAFT
    private Thread automaticDraftThread;
    
    //MLB TEAMS SCREEN VARIABLES
    FlowPane proTeamsToolbar;
    Label proTeamLabel;
    //COMBO BOX FOR PRO TEAMS
    ComboBox proTeamComboBox;
    MLBPlayersTable MLBPlayersTable;
    ObservableList<MLBPlayer> MLBPlayers;
    
    //EVERYTHING ELSE
    
    //IN EACH SCREEN, THERE'S A TEXT AREA WITH THE TITLE.
    Label titleField;

    //THE MAIN SCREEN
    BorderPane borderPane;

    //DRAFT SCREEN, OR WHERE THE MAGIC HAPPENS (WORKSPACE)
    BorderPane draftPane;
    ScrollPane draftScrollPane;
    
    //VBOXES FOR EACH INDIVIDUAL SCREEN
    VBox fantasyTeamsScreen;
    VBox availablePlayersScreen;
    VBox fantasyStandingsScreen;
    VBox draftSummaryScreen;
    VBox MLBTeamsScreen;
    
    //NEED A DRAFT
    WolfieballDraft draft; 
   
    //MANAGES ALL OF THE APPS DATA
    DraftDataManager dataManager;

    //MANAGES COURSE FILE I/O
    DraftFileManager draftFileManager;

    //MANAGES EXPORTING OUR PAGES?
    DraftExporter draftExporter;

    //MANAGES INTERACTIONS WITH FILE-RELATED CONTROLS
    FileController fileController;

    //APPLICATION WINDOW
    Stage stage;

    //STAGE'S SCENE GRAPH
    Scene primaryScene;

    //BUTTONS FOR THE TOP LEFT PORTION(SAVING/LOADING/ETC)
    HBox topLeftContainer;
    Button newDraftButton;
    Button openDraftButton;
    Button saveDraftButton;
    Button exportDraftButton;
    Button exitButton;

    //BUTTONS FOR THE BOTTOM LEFT PORTION OF THE MAIN CLASS
    HBox bottomLeftContainer;
    Button fantasyTeamsScreenButton;
    Button playersScreenButton;
    Button fantasyStandingsScreenButton;
    Button draftScreenButton;
    Button MLBTeamsScreenButton;
    
    //PLAYERS DIALOG
    AddPlayerDialog addNewPlayerDialog;
    EditPlayerDialog editPlayerDialog;
    YesNoCancelDialog removePlayerDialog;
    
    //STRINGS FOR FILE PATHS
    String draftPathH = "C:\\Users\\Tristen\\Documents\\NetBeansProjects\\Wolfieball\\data\\Hitters.json";
    String draftPathP = "C:\\Users\\Tristen\\Documents\\NetBeansProjects\\Wolfieball\\data\\Pitchers.json";
    
    //HERE ARE OUR DIALOGS
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    ProgressDialog progressDialog;
    
    boolean firstTimeRunning = true;

    public Wolfieball_GUI(Stage s)
    {
        group = new ToggleGroup();
        stage = s;
        draft = new WolfieballDraft();
    }

    public void initGUI(String windowTitle) throws IOException {
        // INIT THE DIALOGS
        initDialogs();

        // INIT THE TOOLBAR
        initFileToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
        // TO THE WINDOW YET
        // WE DON'T NEED THIS?... I'M NOT SURE.

        // NOW SETUP THE EVENT HANDLERS
        initEventHandlers();

        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        initWindow(windowTitle);
    }
    /**
     * Initializes the controller class.
     */
    public void initWindow(String windowTitle) {

        //LOADS THE MAIN PANES AND BUTTONS
        borderPane = new BorderPane();
        borderPane.setTop(topLeftContainer);
        primaryScene = new Scene(borderPane, 1280, 800);
        stage.setScene(primaryScene);
        stage.setTitle(windowTitle);
        stage.show();
    }
    public DraftDataManager getDataManager()
    {
        return dataManager;
    }
    public void setDataManager(DraftDataManager initDataManager)
    {
        dataManager = initDataManager;
    }
    public void setDraftFileManager(DraftFileManager initDraftFileManager)
    {
        draftFileManager = initDraftFileManager;
    }
    public void setDraftExporter(DraftExporter initDraftExporter)
    {
        draftExporter = initDraftExporter;
    }

    @Override
    public void reloadDraft(WolfieballDraft draftToReload) {
        
    }
   public Stage getWindow() {
        return stage;
    }

    public MessageDialog getMessageDialog() {
        return messageDialog;
    }

    public YesNoCancelDialog getYesNoCancelDialog() {
        return yesNoCancelDialog;
    }
    public PlayersTable getPlayersTable()
    {
        initPlayersTable();
        return playersTable;
    }
    public ObservableList<FantasyTeam> getFantasyTeams()
    {
        return fantasyTeams;
    }
    public ObservableList<FantasyHitter> getFantasyHitters()
    {
        return fantasyHitters;
    }
    public ObservableList<FantasyPitcher> getFantasyPitchers()
    {
        return fantasyPitchers;
    }
    /**
     * Activates/deactivates toolbar buttons when they can and cannot
     * be used to provide foolproof design.
     * @param saved describes whether the loaded Draft has been saved or not.
     */
    public void updateToolbarControls(boolean saved) {
        //TOGGLES WHETHER THE CURRENT COURSE HAS BEEN SAVED OR NOT.
        saveDraftButton.setDisable(saved);

        //ALL OTHER BUTTONS ARE ALWAYS ENABLED
        //ONCE EDITING THE FIRST COURSE BEGINS.
        openDraftButton.setDisable(false);
        exportDraftButton.setDisable(false);

    }
    public DraftFileManager getFileManager()
    {
        return draftFileManager;
    }
    public FileController getFileController()
    {
        return fileController;
    }
    
    public void showErrorDialog(String message)
    {
        messageDialog.show(message);
    }
    //PRIVATE HELPER METHODS.

    //PRIVATE GENERAL METHODS, MAIN SCREEN FOCUS
    
    //ADDS EVENT HANDLERS FOR THE TOP TOOLBAR.
    private void initEventHandlers() throws IOException
    {
        //SETS BUTTON FUNCTIONS
        fileController = new FileController(draftFileManager, draftExporter, messageDialog, yesNoCancelDialog);
        newDraftButton.setOnAction(e -> {
            fileController.handleNewDraftRequest(this);
            // SINCE WE WANT TO CREATE A NEW TOOLBAR
            // WE SHOULD INITIALIZE IT
            initFunctionToolbar();
            showFantasyScreen();
        });
        openDraftButton.setOnAction(e -> {
            //fileController.handleOpenDraftRequest(this);
        });
        saveDraftButton.setOnAction(e -> {
            //fileController.handleSaveDraftRequest(this, dataManager.getDraft());
        });
        exportDraftButton.setOnAction(e -> {
            //fileController.handleExportDraftRequest(this);
        });
        exitButton.setOnAction(e -> {
            fileController.handleExitRequest(this);
        });
        
    }
    //INITIALIZES THE FILE/TOP RIGHT TOOLBAR.
    
    private void initFileToolbar()
    {
        //BUTTON CONTAINER FOR SAVE, LOAD, ETC.
        topLeftContainer = new HBox();
        topLeftContainer.setLayoutX(0);
        topLeftContainer.setPrefHeight(31.0);
        topLeftContainer.setPrefWidth(600.0);
        

        //INITIALIZE BUTTONS FOR SAVE, LOAD, ETC.
        newDraftButton = new Button("New Draft");
        openDraftButton = new Button("Open Draft");
        saveDraftButton = new Button("Save Draft");
        exportDraftButton = new Button("Export Draft");
        exitButton = new Button("Exit");
        topLeftContainer.getChildren().addAll(newDraftButton,
                                               openDraftButton,
                                               saveDraftButton,
                                               exportDraftButton,
                                               exitButton);
        saveDraftButton.setDisable(true);
        exportDraftButton.setDisable(true);
        
    }
    //INITIALIZES BOTTOM TOOLBAR
    private void initFunctionToolbar()
    {
        //BUTTON CONTAINER FOR FANTASY TEAMS, PLAYERS, ETC.
        bottomLeftContainer = new HBox();
        bottomLeftContainer.setLayoutY(773.0);
        bottomLeftContainer.setPrefHeight(31.0);
        bottomLeftContainer.setPrefWidth(600.0);
        
        
        //INITALIZE BUTTONS FOR FANTASYSCREEN, ETC

        fantasyTeamsScreenButton  = new Button("Fantasy Screen");
        playersScreenButton = new Button("Players");
        fantasyStandingsScreenButton = new Button("Fantasy Standings");
        draftScreenButton = new Button("Draft");
        MLBTeamsScreenButton = new Button("MLB Teams");
        bottomLeftContainer.getChildren().addAll(fantasyTeamsScreenButton,
                                                 playersScreenButton,
                                                 fantasyStandingsScreenButton,
                                                 draftScreenButton,
                                                 MLBTeamsScreenButton);
        borderPane.setBottom(bottomLeftContainer);
        showScreen();
        initTitleField("Fantasy Teams");
        fantasyTeamsScreenButton.setOnAction(e -> {
            showFantasyScreen();
        });
        playersScreenButton.setOnAction(e -> {
            try {
                showPlayersScreen();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        fantasyStandingsScreenButton.setOnAction(e -> {
            showFantasyStandingsScreen();
        });
        draftScreenButton.setOnAction(e -> {
            showDraftScreen();
        });
        MLBTeamsScreenButton.setOnAction(e -> {
            showMLBTeamsScreen();
        });
    }

    //INITIALIZES DIALOGS
    private void initDialogs() {
        messageDialog = new MessageDialog(stage, CLOSE_BUTTON_LABEL);
        addNewPlayerDialog = new AddPlayerDialog(stage, draft);
        yesNoCancelDialog = new YesNoCancelDialog(stage);
    }

    //PRIVATE METHODS DEALING WITH INITIALIZING SCREENS
    
    //SHOWS PLAYERS SCREEN
    private void showPlayersScreen() throws Exception
    {
        draftPane.getChildren().clear();
        initTitleField("Available Players");       
        availablePlayersScreen = new VBox();
        playerControls = new FlowPane();
        playerFilters = new HBox();
        draftScrollPane = new ScrollPane();

        //CONTAINER FOR BUTTONS
        addPlayerButton = new Button("Add Player");
        addPlayerButton.setOnAction(e->{
            addNewPlayerDialog.showAndWait();
            if(addNewPlayerDialog.wasCompleteSelected())
            {
                //ADDS A PLAYER TO THE AVAILABLE PLAYERS LIST
                if(addNewPlayerDialog.handleInput(stage)==null){}
                else
                {
                    MLBPlayer addPlayer = addNewPlayerDialog.handleInput(stage);
                    //CHECKS IF HITTER
                    if(addPlayer.getIsHitter())
                        playersTable.getMLBHitters().add((MLBHitter)addPlayer);
                    //OR IF PITCHER
                    else
                        playersTable.getMLBPitchers().add((MLBPitcher)addPlayer);
                    //THEN REFRESHES TABLE
                    playersTable.loadPlayers();
                    playersTable.showPlayers();
                    //UPDATES DRAFT TO SAVE PLAYERS
                    draft.setMLBPitchers(playersTable.getMLBPitchers());
                    draft.setMLBHitters(playersTable.getMLBHitters());
                    draft.setMLBPlayers(playersTable.getAllMLBPlayers());
                    //CLEARS VALUES FOR THE ADD NEW PLAYER DIALOG
                    addNewPlayerDialog.resetValues();
                    //EDITED, SO WE MARK THE FILE CONTROLLER
                    fileController.markAsEdited(this);
                }
            }
        });
        removePlayerButton = new Button("Remove Player");
        removePlayerButton.setOnAction(e->{
            removeFantasyPlayerDialog = new YesNoCancelDialog(stage);
            removeFantasyPlayerDialog.show("Do you wish to remove this player?");
            if(removeFantasyPlayerDialog.selection.equals("Yes"))
            {
                MLBPlayer mlbToRemove = (MLBPlayer)playersTable.getSelectionModel().getSelectedItem();
                ObservableList<MLBHitter> mlbHitters = draft.getMLBHitters();
                ObservableList<MLBPitcher> mlbPitchers = draft.getMLBPitchers();
                if(mlbToRemove.getIsHitter())
                    mlbHitters.remove(mlbToRemove);
                else
                    mlbPitchers.remove(mlbToRemove);
                draft.setMLBPlayers(mlbHitters, mlbPitchers);
                playersTable.setMLBPlayers(draft.getMLBPlayers());
            }
            else
            {
                //CANCEL/NO, DON'T CARE
            }
        });
        playerControls.getChildren().addAll(addPlayerButton, removePlayerButton);
        
        //SEARCH FUNCTION
        searchLabel = new Label("Search:");
        searchField = new TextField();
        playerControls.getChildren().addAll(searchLabel, searchField);
        
        //INIT PLAYER CONTROLS (SEARCH, ADD/REMOVE PLAYER)
        
        //INITIALIZE TABLE
        initPlayersTable();
        
        //INITS RADIO BUTTONS
        initPlayerScreenRadioButtons();
        
        //SEARCH WITHIN
        searchField.textProperty().addListener(new ChangeListener() {
                public void changed (ObservableValue observable,
                                     Object oldVal,
                                     Object newVal)
                {
                    playerSearch((String)oldVal, (String)newVal);
                }
            });
        availablePlayersScreen.getChildren().addAll(titleField,
                                                    playerControls,
                                                    playerFilters,
                                                    playersTable);
        draftPane.setCenter(availablePlayersScreen);
        draftScrollPane.setContent(draftPane);
        draftScrollPane.setFitToWidth(true);
        borderPane.setCenter(draftScrollPane);
    }
    //SHOWS FANTASY TEAMS SCREEN
    private void showFantasyScreen()
    {
        draftPane.getChildren().clear();
        initTitleField("Fantasy Teams");
        fantasyTeamsScreen = new VBox();
        startingLineupVBox = new VBox();
        taxiSquadVBox = new VBox();
        fantasyControls = new FlowPane();
        draftScrollPane = new ScrollPane();
        addFantasyTeamButton = new Button("Add Team");
        addFantasyTeamButton.setOnAction(e->{
            fantasyController = new FantasyTeamsController(stage, draft);
            if(fantasyTeams == null)
                fantasyTeams = FXCollections.observableArrayList();
            saveTeams();
            fantasyController.handleAddFantasyTeams(this);
            fantasyTeams.setAll(draft.getFantasyTeams());
            saveTeams();
            refreshSelectionControls();
        });
        removeFantasyTeamButton = new Button("Remove Team");
        removeFantasyTeamButton.setOnAction(e->{
            removeFantasyTeamDialog = new YesNoCancelDialog(stage);
            removeFantasyTeamDialog.show("Do you wish to remove this team?");
            if(removeFantasyTeamDialog.selection.equals("Yes"))
            {
                String ftToRemove = (String)fantasyTeamComboBox.getSelectionModel().getSelectedItem();
                FantasyTeam toRemove = null;
                int index = 0;
                for(FantasyTeam ft:fantasyTeams)
                {
                    if(ft.getName().equals(ftToRemove))
                    {
                        toRemove = ft;
                        break;
                    }
                    index++;
                }
                ArrayList<FantasyHitter> fhToRemove = toRemove.getFantasyHitters();
                ArrayList<FantasyPitcher> fpToRemove = toRemove.getFantasyPitchers();
                draftedPlayers.removeAll(fhToRemove);
                draftedPlayers.removeAll(fpToRemove);
                //ADDS ALL THE PLAYERS BACK INTO THE AVAILABLE PLAYERS
                draft.getMLBHitters().addAll(fhToRemove);
                draft.getMLBPitchers().addAll(fpToRemove);
                
                //RELOADS THEM INTO DRAFT, BECAUSE SET WOULD RESET THEM AND MAKE THEM NULL
                ObservableList<MLBHitter> hitters = FXCollections.observableArrayList(draft.getMLBHitters());
                ObservableList<MLBPitcher> pitchers = FXCollections.observableArrayList(draft.getMLBPitchers());
                draft.setMLBHitters(hitters);
                draft.setMLBPitchers(pitchers);
                //RELOAD THE PLAYERS TABLE
                startingLineupTable.setMLBPlayers(draft.getMLBHitters(), draft.getMLBPitchers());
                startingLineupTable.clearPlayers();
                startingLineupTable.loadPlayers();
                
                //REMOVES THE TEAM, SAVES IT, AND REFRESHES THE CONTROLS.
                fantasyTeams.remove(index);
                saveTeams();
                refreshSelectionControls();
            }
            else
            {
                //CANCEL/NO, DON'T CARE
            }
            refreshSelectionControls();
        });
        editFantasyTeamButton = new Button("Edit Team");
        editFantasyTeamButton.setOnAction(e->{
           String ftToEdit = (String)fantasyTeamComboBox.getSelectionModel().getSelectedItem();
           FantasyTeam toEdit = null;
           int index =0;
           for(FantasyTeam ft:fantasyTeams)
           {
               if(ft.getName().equals(ftToEdit))
               {
                   toEdit = new FantasyTeam(ft.getName(), ft.getOwnerName(), ft.getFantasyPitchers(), ft.getFantasyHitters());
                   break;
               }
               index++;
           }
           fantasyController = new FantasyTeamsController(stage, draft);
           //FIND THE INDEX AND THEN REPLACE THE TEAM THERE WITH THE NEW UPDATED TEAM
           fantasyController.handleEditFantasyTeam(this, toEdit);
           fantasyTeams.set(index, toEdit);
           draft.setFantasyTeams(fantasyTeams);
           saveTeams();
           refreshSelectionControls();
        });
    
    //DRAFT TOOLBAR 
        fantasyDraftNameToolbar = new FlowPane();
        draftNameLabel = new Label("Draft Name: ");
        draftNameField = new TextField();
        fantasyDraftNameToolbar.getChildren().addAll(draftNameLabel, draftNameField);
    
    //SELECT FANTASY TEAM
        //DEALS WITH COMBO BOXES.
        
        initFantasyComboBox();
        selectFantasyTeamLabel = new Label("Select Fantasy Team");
        fantasyControls.getChildren().addAll(addFantasyTeamButton,
                                             removeFantasyTeamButton,
                                             editFantasyTeamButton,
                                             selectFantasyTeamLabel,
                                             fantasyTeamComboBox);
    //STARTING LINEUP TABLE
        startingLineupLabel = new Label("Starting Lineup");
        initStartingLineupTable();
        startingLineupLabel.setFont(new Font("Arial", 30));
        startingLineupVBox.getChildren().addAll(startingLineupLabel, 
                                             startingLineupTable);
        
        //TAXI SQUAD TABLE
        taxiSquadLabel = new Label("Taxi Squad");
        taxiSquadTable = new FantasyTable();
        taxiSquadTable.initialize();
        initTaxiRosterTable();
        taxiSquadLabel.setFont(new Font("Arial", 30));
        taxiSquadVBox.getChildren().addAll(taxiSquadLabel,
                                           taxiSquadTable);
    //FANTASY TEAMS DIALOG
        fantasyTeamDialog = new FantasyTeamDialog(stage, draft);
        editFantasyPlayerDialog = new EditPlayerDialog(stage, draft);
        removeFantasyPlayerDialog = new YesNoCancelDialog(stage);
        
        //SETTING THE SCREEN
        fantasyTeamsScreen.getChildren().addAll(titleField,
                                                fantasyDraftNameToolbar,
                                                fantasyControls,
                                                startingLineupVBox,
                                                taxiSquadVBox);
        if(fantasyTeamComboBox.getSelectionModel().getSelectedItem()==null)
        {
            removeFantasyTeamButton.setDisable(true);
            editFantasyTeamButton.setDisable(true);
        }
        draftPane.setCenter(fantasyTeamsScreen);
        draftScrollPane.setContent(draftPane);
        draftScrollPane.setFitToWidth(true);
        borderPane.setCenter(draftScrollPane);                
    }
    //SHOWS THE FANTASY STANDINGS SCREEN
    private void showFantasyStandingsScreen()
    {
        draftPane.getChildren().clear();
        initTitleField("Fantasy Standings");
        fantasyStandingsScreen = new VBox();
        initFantasyStandingsTable();
        fantasyStandingsScreen.getChildren().addAll(titleField,
                                                    standingsTable);
        draftPane.setCenter(fantasyStandingsScreen);
        draftScrollPane.setContent(draftPane);
        draftScrollPane.setFitToWidth(true);
        borderPane.setCenter(draftScrollPane);
    }
    //SHOWS THE DRAFT SCREEN
    private void showDraftScreen()
    {
        draftPane.getChildren().clear();
        initTitleField("Draft Summary");
        draftSummaryScreen = new VBox();
        draftPane.setCenter(draftSummaryScreen);
        draftToolbar = new FlowPane();
        selectPlayerButton = new Button("Select Player");
        selectPlayerButton.setOnAction(e->
        {
            handleSelectPlayer();
        });
        startAutoDraft = new Button("Start Auto Draft");
        startAutoDraft.setOnAction(e->
        {
            automaticDraftThread = new Thread(
            () -> {
                while(true)
                {
                    handleSelectPlayer();
                    try
                    {
                        Thread.sleep(500);
                    }
                    catch(Exception f)
                    {
                        f.printStackTrace();
                    }
                }
            });
            automaticDraftThread.start();
        });
        pauseAutoDraft = new Button("Pause Auto Draft");
        pauseAutoDraft.setOnAction(e -> 
        {
            automaticDraftThread.stop();
        });
        draftToolbar.getChildren().addAll(selectPlayerButton,
                                       startAutoDraft,
                                       pauseAutoDraft);
        initDraftTable();
        draftSummaryScreen.getChildren().addAll(titleField,
                                                draftToolbar,
                                                draftTable);
        draftScrollPane.setContent(draftPane);
        draftScrollPane.setFitToWidth(true);
        borderPane.setCenter(draftScrollPane); 
    }
    //SHOWS THE MLB TEAMS SCREEN
    private void showMLBTeamsScreen()
    {
        draftPane.getChildren().clear();
        initTitleField("MLB Teams");
        MLBTeamsScreen = new VBox();
        MLBTeamsScreen.getChildren().addAll(titleField);
        proTeamsToolbar = new FlowPane();
        proTeamLabel = new Label("Select Pro Teams");
        initProTeamComboBox();
        initProTeamsTable();
        MLBTeamsScreen.getChildren().addAll(proTeamsToolbar,
                                            MLBPlayersTable);
        draftPane.setCenter(MLBTeamsScreen);
        draftScrollPane.setContent(draftPane);
        draftScrollPane.setFitToWidth(true);
        borderPane.setCenter(draftScrollPane); 
    }
    //SHOWS THE MAIN SCRENE
    private void showScreen()
    {
        //INITIALIZE MAIN SCREEN
        draftPane = new BorderPane();
        draftPane.setPrefWidth(1280);
        draftPane.setPrefHeight(743);
        draftPane.setLayoutX(4);
        draftPane.setLayoutY(31);
        borderPane.setCenter(draftPane);
    }
    private void initTitleField(String text)
    {
        //SETS TITLE FIELD
        titleField = new Label();
        titleField.setFont(new Font("Arial", 30));
        titleField.setText(text);
    }
    
    //PRIVATE METHODS FOR PLAYERS SCREEN ONLY
    
    //INITIALIZES FUNCTIONS FOR RADIO BUTTONS, ONLY FOR PLAYERS SCREEN
    private void initPlayerScreenRadioButtons()
    {
        allPlayers = new RadioButton("All");
        allPlayers.setToggleGroup(group);
        allPlayers.setOnAction(e ->
        {
            playersTable.clearPlayers();
            playersTable.setMLBHitters(draft.getMLBHitters());
            playersTable.setMLBPitchers(draft.getMLBPitchers());
            playersTable.getFilteredMLBPlayers("All");
            playersTable.showPlayers();
            searchField.setText("");
        });
        catchers = new RadioButton("C");
        catchers.setToggleGroup(group);
        catchers.setOnAction(e ->
        {
            playersTable.clearPlayers();
            playersTable.setMLBHitters(draft.getMLBHitters());
            playersTable.setMLBPitchers(draft.getMLBPitchers());
            playersTable.getFilteredMLBPlayers("C");
            playersTable.showPlayers();
            searchField.setText("");
        });
        firstBasemen = new RadioButton("1B");
        firstBasemen.setToggleGroup(group);
        firstBasemen.setOnAction(e ->
        {
            playersTable.clearPlayers();
            playersTable.setMLBHitters(draft.getMLBHitters());
            playersTable.setMLBPitchers(draft.getMLBPitchers());
            playersTable.getFilteredMLBPlayers("1B");
            playersTable.showPlayers();
            searchField.setText("");
        });
        cornerInfielders = new RadioButton("CI");
        cornerInfielders.setToggleGroup(group);
        cornerInfielders.setOnAction(e ->
        {
            playersTable.clearPlayers();
            playersTable.setMLBHitters(draft.getMLBHitters());
            playersTable.setMLBPitchers(draft.getMLBPitchers());
            playersTable.getFilteredMLBPlayers("CI");
            playersTable.showPlayers();
            searchField.setText("");
        });
        thirdBasemen = new RadioButton("3B");
        thirdBasemen.setToggleGroup(group);
        thirdBasemen.setOnAction(e ->
        {
            playersTable.clearPlayers();
            playersTable.setMLBHitters(draft.getMLBHitters());
            playersTable.setMLBPitchers(draft.getMLBPitchers());
            playersTable.getFilteredMLBPlayers("3B");
            playersTable.showPlayers();
            searchField.setText("");
        });
        secondBasemen = new RadioButton("2B");
        secondBasemen.setToggleGroup(group);
        secondBasemen.setOnAction(e ->
        {
            playersTable.clearPlayers();
            playersTable.setMLBHitters(draft.getMLBHitters());
            playersTable.setMLBPitchers(draft.getMLBPitchers());
            playersTable.getFilteredMLBPlayers("2B");
            playersTable.showPlayers();
            searchField.setText("");
        });
        middleInfielders = new RadioButton("MI");
        middleInfielders.setToggleGroup(group);
        middleInfielders.setOnAction(e ->
        {
            playersTable.clearPlayers();
            playersTable.setMLBHitters(draft.getMLBHitters());
            playersTable.setMLBPitchers(draft.getMLBPitchers());
            playersTable.getFilteredMLBPlayers("MI");
            playersTable.showPlayers();
            searchField.setText("");
        });
        shortstop = new RadioButton("SS");
        shortstop.setToggleGroup(group);
        shortstop.setOnAction(e ->
        {
            playersTable.clearPlayers();
            playersTable.setMLBHitters(draft.getMLBHitters());
            playersTable.setMLBPitchers(draft.getMLBPitchers());
            playersTable.getFilteredMLBPlayers("SS");
            playersTable.showPlayers();
            searchField.setText("");
        });
        outfielders = new RadioButton("OF");
        outfielders.setToggleGroup(group);
        outfielders.setOnAction(e ->
        {
            playersTable.clearPlayers();
            playersTable.setMLBHitters(draft.getMLBHitters());
            playersTable.setMLBPitchers(draft.getMLBPitchers());
            playersTable.getFilteredMLBPlayers("OF");
            playersTable.showPlayers();
            searchField.setText("");
        });
        utilityPlayers = new RadioButton("U");
        utilityPlayers.setToggleGroup(group);
        utilityPlayers.setOnAction(e ->
        {
            playersTable.clearPlayers();
            playersTable.setMLBHitters(draft.getMLBHitters());
            playersTable.setMLBPitchers(draft.getMLBPitchers());
            playersTable.getFilteredMLBPlayers("U");
            playersTable.showPlayers();
            searchField.setText("");
        });
        pitchers = new RadioButton("P");
        pitchers.setToggleGroup(group);
        pitchers.setOnAction(e ->
        {
            playersTable.clearPlayers();
            playersTable.setMLBHitters(draft.getMLBHitters());
            playersTable.setMLBPitchers(draft.getMLBPitchers());
            playersTable.getFilteredMLBPlayers("P");
            playersTable.showPlayers();
            searchField.setText("");
        });
        allPlayers.setSelected(true);
        playerFilters.getChildren().addAll(allPlayers,
                                            catchers,
                                            firstBasemen,
                                            cornerInfielders,
                                            thirdBasemen,
                                            secondBasemen,
                                            middleInfielders,
                                            shortstop,
                                            outfielders,
                                            utilityPlayers,
                                            pitchers);
    }
    private void initPlayersTable()
    {
        playersTable = new PlayersTable();
        playersTable.initialize();
        dataManager = new DraftDataManager(this);
        dataManager.setDraft(draft);
        apc = new AvailablePlayersController(stage, dataManager.getDraft());
        playersTable.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) 
            {
                // OPEN UP THE EDITOR
                MLBPlayer player= (MLBPlayer)playersTable.getSelectionModel().getSelectedItem();
                MLBHitter h;
                MLBPitcher p;
                ObservableList<MLBHitter> mlbHitters = draft.getMLBHitters();
                ObservableList<MLBPitcher> mlbPitchers = draft.getMLBPitchers();
                if(draftedPlayers == null)
                    draftedPlayers = FXCollections.observableArrayList();
                if(fantasyTeams.isEmpty())
                    showErrorDialog("There are no Fantasy Teams. Please create a fantasy team first.");
                else
                {
                    if(player.getIsHitter())
                    {
                        h = (MLBHitter)player;
                        apc.handleEditPlayerRequest(this, h);
                        editPlayerDialog = new EditPlayerDialog(stage, draft);
                        draftedPlayers.add(player);
                    }
                    else
                    {
                        p = (MLBPitcher)player;
                        apc.handleEditPlayerRequest(this, p);
                        editPlayerDialog = new EditPlayerDialog(stage, draft);
                        draftedPlayers.add(player);
                    }
                    draft.setMLBPlayers(mlbHitters, mlbPitchers);
                    draft.setDraftedPlayers(draftedPlayers);
                    RadioButton check = (RadioButton)group.getSelectedToggle();
                    if(check.getText().equals("All"))
                        playersTable.setMLBPlayers(draft.getMLBPlayers());
                    else
                    {
                        ObservableList<MLBPlayer> filteredPlayers = FXCollections.observableArrayList(playersTable.getFilteredMLBPlayers(check.getText()));
                        playersTable.setMLBPlayers(filteredPlayers);
                    }
                    playersTable.showPlayers();
                }
            }        
        });
        //INIT ON FIRST TIME
        if(firstTimeRunning)
        {
            playersTable.loadPlayers(draftPathH, draftPathP);
            draft.setMLBPitchers(playersTable.getMLBPitchers());
            draft.setMLBHitters(playersTable.getMLBHitters());
            draft.setMLBPlayers(playersTable.getAllMLBPlayers());
            firstTimeRunning = false;
        }
        else
        {
            playersTable.setMLBPitchers(draft.getMLBPitchers());
            playersTable.setMLBHitters(draft.getMLBHitters());
            playersTable.loadPlayers();
        }
        playersTable.showPlayers();
        draft.setMLBPlayers(playersTable.getAllMLBPlayers());
    }
    /**
     * Applies the search using the text field to the players.
     * @param oldValue the value before input
     * @param newValue the value after input
     */
    private void playerSearch(String oldValue, String newValue)
    {
        //APPLIES SEARCH THROUGH LIST
        if(oldValue != null && newValue.length() < oldValue.length())
        {
            playersTable.clearPlayers();
            playersTable.setMLBHitters(draft.getMLBHitters());
            playersTable.setMLBPitchers(draft.getMLBPitchers());
        }
        playersTable.setMLBHitters(draft.getMLBHitters());
        playersTable.setMLBPitchers(draft.getMLBPitchers());
        RadioButton check = (RadioButton)group.getSelectedToggle();
        ObservableList<MLBPlayer> players = playersTable.getFilteredMLBPlayers(check.getText());
        newValue = newValue.toUpperCase();
        ObservableList<MLBHitter> searchHitters= FXCollections.observableArrayList();
        ObservableList<MLBPitcher> searchPitchers= FXCollections.observableArrayList();
        
        //ADDS PLAYERS TO NEW OBSERVABLE LIST
        for(MLBPlayer player : players)
        {
            if(player.getFirstName().toUpperCase().startsWith(newValue)
                    ||player.getLastName().toUpperCase().startsWith(newValue))
            {
                if(!player.getIsHitter())
                    searchPitchers.add((MLBPitcher)player);
                else
                    searchHitters.add((MLBHitter)player);
            }
        }
        //REFRESHES TABLE TO UPDATE
        playersTable.setMLBHitters(searchHitters);
        playersTable.setMLBPitchers(searchPitchers);
        playersTable.loadPlayers();
        playersTable.showPlayers();
    }
    
    //PRIVATE METHODS FOR FANTASY TEAMS ONLY

    private void initFantasyComboBox() {
        fantasyTeams = FXCollections.observableArrayList(draft.getFantasyTeams());
        ObservableList<String> obsFTString = FXCollections.observableArrayList();
        for(FantasyTeam ft: fantasyTeams)
            obsFTString.add(ft.getName());
        if(fantasyTeams==null)
            fantasyTeams = FXCollections.observableArrayList();
        fantasyTeamComboBox = new ComboBox(obsFTString);
        fantasyTeamComboBox.getSelectionModel().selectFirst();
        fantasyTeamComboBox.setOnAction(e->{
            for(FantasyTeam ft: fantasyTeams)
            {
                if(((String)fantasyTeamComboBox.getSelectionModel().getSelectedItem()).equals(ft.getName()))
                {
                    startingLineupTable.setFantasyHitters(FXCollections.observableArrayList(ft.getFantasyHitters()));
                    startingLineupTable.setFantasyPitchers(FXCollections.observableArrayList(ft.getFantasyPitchers()));
                    startingLineupTable.loadPlayers();
                    startingLineupTable.showPlayers();
                    taxiSquadTable.setFantasyHitters(FXCollections.observableArrayList(ft.getTaxiRosterHitters()));
                }
            }
        });
    }
    private void refreshSelectionControls()
    {
        initFantasyComboBox();
        if(fantasyTeamComboBox.getSelectionModel().getSelectedItem()==null)
        {
            removeFantasyTeamButton.setDisable(true);
            editFantasyTeamButton.setDisable(true);
        }
        else
        {
            removeFantasyTeamButton.setDisable(false);
            editFantasyTeamButton.setDisable(false);
        }
        fantasyControls.getChildren().clear();
        fantasyControls.getChildren().addAll(addFantasyTeamButton,
                                            removeFantasyTeamButton,
                                            editFantasyTeamButton,
                                            selectFantasyTeamLabel,
                                            fantasyTeamComboBox);
        fantasyTeamsScreen.getChildren().set(2, fantasyControls);
    }
    private void initStartingLineupTable()
    {
        startingLineupTable = new FantasyTable();
        startingLineupTable.initialize();
        startingLineupTable.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) 
            {
                fantasyController = new FantasyTeamsController(stage, draft);
                // OPEN UP THE EDITOR
                MLBPlayer player = (MLBPlayer)startingLineupTable.getSelectionModel().getSelectedItem();
                FantasyHitter h;
                FantasyPitcher p;
                
                if(player.getIsHitter())
                {
                    h = (FantasyHitter)player;
                    draftedPlayers.remove(h);
                    fantasyController.handleEditFantasyPlayer(this, h);
                    efpd = new EditFantasyPlayerDialog(stage, draft);
                    draftedPlayers.add(h);
                }
                else
                {
                    p = (FantasyPitcher)player;
                    draftedPlayers.remove(p);
                    fantasyController.handleEditFantasyPlayer(this, p);
                    efpd = new EditFantasyPlayerDialog(stage, draft);
                    draftedPlayers.add(p);
                }
                loadFantasyTeamsTable();
            }        
        });
        fantasyTeams = FXCollections.observableArrayList(draft.getFantasyTeams());
        fantasyTeamComboBox.getSelectionModel().selectFirst();
        if(fantasyTeams!=null&&fantasyTeamComboBox.getSelectionModel().getSelectedItem()!=null)
            loadFantasyTeamsTable();
        //ELSE, WE DO NOTHING
    }
    private void initTaxiRosterTable()
    {
        taxiSquadTable = new FantasyTable();
        taxiSquadTable.initialize();
        fantasyTeams = FXCollections.observableArrayList(draft.getFantasyTeams());
//        if(fantasyTeams!=null&&fantasyTeamComboBox.getSelectionModel().getSelectedItem()!=null)
//        {
//            String fantasyTeamName =  (String)fantasyTeamComboBox.getSelectionModel().getSelectedItem();
//            for(FantasyTeam ft: fantasyTeams)
//                if(ft.getName().equals(fantasyTeamName))
//                {
//                    taxiSquadTable.setFantasyHitters(FXCollections.observableArrayList(ft.getTaxiRosterHitters()));
//                }
//            taxiSquadTable.loadTaxiPlayers();
//            taxiSquadTable.showPlayers();
//        }
    }
    private void saveTeams()
    {
        draft.setFantasyTeams(fantasyTeams);
        dataManager.setDraft(draft);
    }
    
    private void loadFantasyTeamsTable()
    {
        for(FantasyTeam ft: fantasyTeams)
        {
            if(((String)fantasyTeamComboBox.getSelectionModel().getSelectedItem()).equals(ft.getName()))
            {
                startingLineupTable.setFantasyHitters(FXCollections.observableArrayList(ft.getFantasyHitters()));
                startingLineupTable.setFantasyPitchers(FXCollections.observableArrayList(ft.getFantasyPitchers()));
                startingLineupTable.loadPlayers();
                startingLineupTable.showPlayers();
            }
        }
    }
    //PRIVATE METHODS FOR DRAFT SCREEN
    private void initDraftTable()
    {
        draftTable = new DraftTable();
        draftTable.initialize();
        if(draftedPlayers == null)
            draftedPlayers = FXCollections.observableArrayList(draft.getDraftedPlayers());
        draftTable.setFantasyHitters(draft.getDraftedHitters());
        draftTable.setFantasyPitchers(draft.getDraftedPitchers());
        draftTable.loadAllDraftedPlayers();
        draftTable.showPlayers();
    }
    private void handleSelectPlayer()
    {
        ObservableList<FantasyTeam> fantasyTeams = draft.getFantasyTeams();
        ObservableList<MLBHitter> mlbHitters = draft.getMLBHitters();
        ObservableList<MLBPitcher> mlbPitchers = draft.getMLBPitchers();
        ObservableList<MLBHitter> mlbHittersToRemove = FXCollections.observableArrayList();
        ObservableList<MLBPitcher> mlbPitchersToRemove = FXCollections.observableArrayList();
        FantasyTeam ft = null;
        int index = 0;
        for(FantasyTeam team: fantasyTeams)
        {
           if(team.needsPlayers())
            {
                ft = team;
                break;
            }
            index++;
        }
        if(ft != null)
        {
            int size = 0;
            boolean ranAlready = false;
            outerLoop:
            for(MLBHitter h : mlbHitters)
            {
                String[] qp =  h.getQualifyingPositions();
                //NUM OF PITCHERS
                if(ft.getPlayersNeeded()!=9)
                {
                    for(String s : qp)
                    {
                        if(ft.isAvailable(s))
                        {
                            FantasyHitter fh = new FantasyHitter(h, 1, ft.getName(), 2);
                            fh.setCurrentPosition(s);
                            draft.getDraftedHitters().add(fh);
                            draft.setDraftedPlayers();
                            size = draft.getDraftedPlayers().size();
                            fh.setPickNumber(size);
                            ft.getFantasyHitters().add(fh);
                            ft.setPlayersNeeded(ft.getPlayersNeeded()-1);
                            mlbHittersToRemove.add(h);
                            ranAlready = true;
                            break outerLoop;
                        }
                    }
                }
            }
            mlbHitters.removeAll(mlbHittersToRemove);
            if(ft.getPlayersNeeded()<=9&&!ranAlready)
            {
                outerLoop:
                for(MLBPitcher p : mlbPitchers)
                {
                    if(ft.isAvailable("P"))
                    {
                        FantasyPitcher fp = new FantasyPitcher(p, 1, ft.getName(), 2);
                        ft.getFantasyPitchers().add(fp);
                        ft.setPlayersNeeded(ft.getPlayersNeeded()-1);
                        draft.getDraftedPitchers().add(fp);
                        draft.setDraftedPlayers();
                        size = draft.getDraftedPlayers().size();
                        fp.setPickNumber(size);
                        mlbPitchersToRemove.add(p);
                        break outerLoop;
                    }
                }
            }
            mlbPitchers.removeAll(mlbPitchersToRemove);
            draft.setDraftedPlayers();
            draftTable.setAllDraftedPlayers(draft.getDraftedPlayers());
            draft.getFantasyTeams().set(index, ft);
        }
        else
        {
            for(FantasyTeam team: fantasyTeams)
            {
               if(!team.isTaxiRosterFull())
               {
                    ft = team;
                    break;
               }
            }
            MLBHitter hitter = mlbHitters.get(0);
            FantasyHitter fh = new FantasyHitter(hitter, 1, ft.getName(), 0);
            ft.getTaxiRosterHitters().add(fh);
            mlbHittersToRemove.add(hitter); 
//            draft.getFantasyTeams().set(index, ft);
        }
    }
    
    //PRIVATE METHODS FOR FANTASY STANDINGS
    private void initFantasyStandingsTable()
    {
        standingsTable = new FantasyStandingsTable();
        standingsTable.initialize();
        standingsTable.setTeams(draft.getFantasyTeams());
        standingsTable.loadTeams();
    }
    //PRIVATE METHODS FOR MLB TEAMS
    private void initProTeamComboBox()
    {
        ObservableList<String> options = FXCollections.observableArrayList("ATL", 
                                                                           "AZ", 
                                                                           "CHC", 
                                                                           "CIN", 
                                                                           "COL", 
                                                                           "LAD", 
                                                                           "MIA", 
                                                                           "MIL", 
                                                                           "NYM", 
                                                                           "PHI", 
                                                                           "PIT", 
                                                                           "SD", 
                                                                           "SF", 
                                                                           "STL", 
                                                                           "WAS");
        proTeamComboBox = new ComboBox(options);
        proTeamComboBox.setValue("");
        proTeamsToolbar.getChildren().addAll(proTeamLabel, proTeamComboBox);
        proTeamComboBox.setOnAction(e->{
            setMLBPlayers();
            MLBPlayersTable.setMLBPlayers(MLBPlayers);
            MLBPlayersTable.showPlayers();
        });
    }
    private void initProTeamsTable()
    {
        MLBPlayersTable = new MLBPlayersTable();
        MLBPlayersTable.initialize();
        proTeamComboBox.getSelectionModel().selectFirst();
        setMLBPlayers();
        MLBPlayersTable.setMLBPlayers(MLBPlayers);
        MLBPlayersTable.showPlayers();
    }
    private void setMLBPlayers()
    {
        ObservableList<MLBPlayer> allPlayers = FXCollections.observableArrayList(draft.getMLBPlayers());
        MLBPlayers = FXCollections.observableArrayList();
        for(MLBPlayer p:allPlayers)
            if(p.getTeam().equals(proTeamComboBox.getSelectionModel().getSelectedItem()))
                MLBPlayers.add(p);
        if(fantasyTeams!=null)
        {
            for(FantasyTeam ft: fantasyTeams)
            {
                for(MLBPlayer h:ft.getFantasyHitters())
                    if(h.getTeam().equals(proTeamComboBox.getSelectionModel().getSelectedItem()))
                        MLBPlayers.add(h);
                for(MLBPlayer p:ft.getFantasyPitchers())
                    if(p.getTeam().equals(proTeamComboBox.getSelectionModel().getSelectedItem()))
                        MLBPlayers.add(p);
            }
        }
    }
    
}
