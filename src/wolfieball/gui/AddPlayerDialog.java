
package wolfieball.gui;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wolfieball.mlb.MLBHitter;
import wolfieball.mlb.MLBPitcher;
import wolfieball.mlb.MLBPlayer;
import wolfieball.controller.AvailablePlayersController;
import wolfieball.data.WolfieballDraft;

/**
 *
 * @author Tristen
 */
public class AddPlayerDialog extends Stage{
    
    //THE OBJECT OF THIS UI
    MLBPlayer player;
    VBox bodyPane;
    Scene bodyScene;
    //ALL LABELS AND TEXT FIELDS
    Label playerDetails;
    Label firstNameLabel;
    Label lastNameLabel;
    Label proTeamLabel;
    TextField firstNameTextField;
    TextField lastNameTextField;
    
    //FLOW PANES FOR NAMES
    FlowPane firstNameToolbar;
    FlowPane lastNameToolbar;
    FlowPane proTeamToolbar;
    FlowPane checkBoxToolbar;
    FlowPane completeCancelToolbar;
    
    //COMBO BOX FOR PRO TEAMS
    ComboBox proTeamComboBox;
    
    //CHECK BOXES FOR QUALIFYING POSITIONS
    CheckBox catcherCheckBox;
    CheckBox firstBaseCheckBox;
    CheckBox thirdBaseCheckBox;
    CheckBox secondBaseCheckBox;
    CheckBox shortStopCheckBox;
    CheckBox outfieldCheckBox;
    CheckBox pitcherCheckBox;
    
    //BUTTONS TO COMPLETE/CANCEL
    Button completeButton;
    Button cancelButton;
    
    //TO GET THE MLB PLAYER LIST
    WolfieballDraft draft;
    PlayersTable playersTable;
    
    //SOME VARIABLES WE'LL NEED FOR ADDING THE PLAYER AND IT'S DATA.
    String[] qualifyingPositions;
    ObservableList<MLBPlayer> players;
    
    //WE'LL NEED A CONTROLLER TO CONTROL THE DATA
    AvailablePlayersController playersController;
    
    private String selection;
    MessageDialog messageDialog;
    
    //ADDS A NEW PLAYER FOR REGULAR TEAMS ONLY, IE MLB TEAMS
    public AddPlayerDialog(Stage owner, WolfieballDraft initDraft)
    {
        // MAKE IT MODAL
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);
        bodyPane = new VBox();
        bodyPane.setPrefWidth(400);
        bodyPane.setPrefHeight(200);
        bodyScene = new Scene(bodyPane);
        this.setTitle("Add Player Dialog");
        draft = initDraft;
        players = draft.getMLBPlayers();
        playerDetails = new Label("Player Details");
        playerDetails.setFont(new Font("Arial", 30));
        firstNameLabel = new Label("First Name: " );
        lastNameLabel = new Label("Last Name: " );
        proTeamLabel = new Label("Pro Team: ");
        firstNameTextField = new TextField();
        lastNameTextField = new TextField();
        firstNameTextField.setText("");
        lastNameTextField.setText("");
        firstNameToolbar = new FlowPane();
        firstNameToolbar.getChildren().addAll(firstNameLabel, firstNameTextField);
        selection = "";
        lastNameToolbar = new FlowPane();
        lastNameToolbar.getChildren().addAll(lastNameLabel, lastNameTextField);
        
        proTeamToolbar = new FlowPane();
        initProTeamComboBox();
        
        checkBoxToolbar = new FlowPane();
        initPositionsCheckBox();
        
        completeButton = new Button("Complete");
        cancelButton = new Button("Cancel");
        
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            AddPlayerDialog.this.selection = sourceButton.getText();
            AddPlayerDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);
        completeCancelToolbar = new FlowPane();
        completeCancelToolbar.getChildren().addAll(completeButton, cancelButton);
        bodyPane.getChildren().addAll(playerDetails, 
                                      firstNameToolbar, 
                                      lastNameToolbar, 
                                      proTeamToolbar, 
                                      checkBoxToolbar,
                                      completeCancelToolbar);
        this.setScene(bodyScene);
    }
    private void initProTeamComboBox()
    {
        ObservableList<String> options = FXCollections.observableArrayList("ATL", 
                                                                           "AZ", 
                                                                           "CHC", 
                                                                           "CIN", 
                                                                           "COL", 
                                                                           "LAD", 
                                                                           "MIA", 
                                                                           "MIL", 
                                                                           "NYM", 
                                                                           "PHI", 
                                                                           "PIT", 
                                                                           "SD", 
                                                                           "SF", 
                                                                           "STL", 
                                                                           "WAS");
        proTeamComboBox = new ComboBox(options);
        proTeamComboBox.setValue("");
        proTeamToolbar.getChildren().addAll(proTeamLabel, proTeamComboBox);
    }
    private void initPositionsCheckBox()
    {
        catcherCheckBox = new CheckBox("C");
        catcherCheckBox.setSelected(false);
        firstBaseCheckBox = new CheckBox("1B");
        firstBaseCheckBox.setSelected(false);
        thirdBaseCheckBox = new CheckBox("3B");
        thirdBaseCheckBox.setSelected(false);
        secondBaseCheckBox = new CheckBox("2B");
        secondBaseCheckBox.setSelected(false);
        shortStopCheckBox = new CheckBox("SS");
        shortStopCheckBox.setSelected(false);
        outfieldCheckBox = new CheckBox("OF");
        outfieldCheckBox.setSelected(false);
        pitcherCheckBox = new CheckBox("P");
        pitcherCheckBox.setSelected(false);
        checkBoxToolbar.getChildren().addAll(catcherCheckBox,
                                             firstBaseCheckBox,
                                             thirdBaseCheckBox,
                                             secondBaseCheckBox,
                                             shortStopCheckBox,
                                             outfieldCheckBox,
                                             pitcherCheckBox);
        
    }
    //SINCE WE'RE ADDING IT TO THE PLAYERS, WE NEED TO HANDLE THE INPUT
    public MLBPlayer handleInput(Stage stage)
    {
        //HANDLE INPUT, SINCE THEY DIDN'T CANCEL
        //IS THE PITCHER BOX SELECTED?
        handleCheckBoxes();
        if(proTeamComboBox.getSelectionModel().getSelectedItem()==null
                ||proTeamComboBox.getSelectionModel().getSelectedItem().equals("")
                ||createQualifyingPositionsString().equals("U")
                ||firstNameTextField.getText().equals("")
                ||lastNameTextField.getText().equals(""))
        {
            messageDialog = new MessageDialog(stage, "OK");
            messageDialog.show("Either one or all of your input value was incorrect. No new player was created.");
            return null;
        }
        else
        {
            if(pitcherCheckBox.isSelected())
            {
                MLBPitcher mlbPlayer = new MLBPitcher();
                mlbPlayer.setName(firstNameTextField.getText(), lastNameTextField.getText());
                mlbPlayer.setTeam((String)proTeamComboBox.getSelectionModel().getSelectedItem());
                mlbPlayer.setRole("P");
                mlbPlayer.setNotes(createQualifyingPositionsString());
                return mlbPlayer;
            }
            //NO, HITTER THEN.
            else
            {
                MLBHitter mlbPlayer = new MLBHitter();
                mlbPlayer.setName(firstNameTextField.getText(), lastNameTextField.getText());
                mlbPlayer.setTeam((String)proTeamComboBox.getSelectionModel().getSelectedItem());
                mlbPlayer.setQualifyingPosition(createQualifyingPositionsString());
                mlbPlayer.setNotes(createQualifyingPositionsString());
                return mlbPlayer;
            }
        } 
    }
    private void handleCheckBoxes()
    {
        qualifyingPositions = new String[7];
        int index = 0;
        if(pitcherCheckBox.isSelected())
            qualifyingPositions[index] = "P";
        else
        {
            boolean isCornerInfielder = false;
            boolean isMiddleInfielder = false;
            if(catcherCheckBox.isSelected())
            {
                qualifyingPositions[index] = "C";
                index++;
            }
            if(firstBaseCheckBox.isSelected())
            {
                qualifyingPositions[index] = "1B";
                isCornerInfielder = true;
                index++;
            }
            if(thirdBaseCheckBox.isSelected())
            {
                qualifyingPositions[index] = "3B";
                isCornerInfielder = true;
                index++;
            }
            if(secondBaseCheckBox.isSelected())
            {
                qualifyingPositions[index] = "2B";
                isMiddleInfielder = true;
                index++;
            }
            if(shortStopCheckBox.isSelected())
            {
                qualifyingPositions[index] = "SS";
                isMiddleInfielder = true;
                index++;
            }
            if(outfieldCheckBox.isSelected())
            {
                qualifyingPositions[index] = "OF";
                index++;
            }
            if(isCornerInfielder)
            {
                qualifyingPositions[index] = "CI";
                index++;
            }
            if(isMiddleInfielder)
            {
                qualifyingPositions[index] = "MI";
                index++;
            }
            qualifyingPositions[index] = "U";
        }
    }
    public String createQualifyingPositionsString()
    {
        String qpString = "";
        for(String s: qualifyingPositions)
        {
            if(s!=null) {
                qpString+=s + "_";
            }
        }
        qpString = qpString.substring(0, qpString.length()-1);//TRIMS _ FROM IT, OR U_ = U
        return qpString;
    }
     /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     * 
     * @param message Message to appear inside the dialog.
     */
    public void show(String message) {
        
        playerDetails.setText(message);
        this.showAndWait();
    }
    public boolean wasCompleteSelected()
    {
        return selection.equals("Complete");
    }
    public String getSelection(){
        return selection;
    }
    public void resetValues()
    {
        firstNameTextField.setText("");
        lastNameTextField.setText("");
        catcherCheckBox.setSelected(false);
        firstBaseCheckBox.setSelected(false);
        thirdBaseCheckBox.setSelected(false);
        secondBaseCheckBox.setSelected(false);
        shortStopCheckBox.setSelected(false);
        outfieldCheckBox.setSelected(false);
        pitcherCheckBox.setSelected(false);
        proTeamComboBox.setValue("");
    }
}
