package wolfieball.gui;

import java.util.Comparator;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.Callback;
import wolfieball.data.FantasyHitter;
import wolfieball.data.FantasyPitcher;
import wolfieball.file.JsonDraftFileManager;
import wolfieball.mlb.MLBHitter;
import wolfieball.mlb.MLBPitcher;
import wolfieball.mlb.MLBPlayer;

/**
 *
 * @author Tristen
 */
public class DraftTable extends TableView
{
    private TableColumn pickNumberColumn;
    private TableColumn firstNameColumn;
    private TableColumn lastNameColumn;
    private TableColumn fantasyTeamColumn;
    private TableColumn contractsColumn;
    private TableColumn salaryColumn;
    private ObservableList<FantasyHitter> fantasyHitters;
    private ObservableList<FantasyPitcher> fantasyPitchers;
    private ObservableList<MLBPlayer> mlbPlayers;
    private JsonDraftFileManager fileManager = new JsonDraftFileManager();
    
    public DraftTable()
    {
        super();
    }
    public void initialize()
    {
        //ADDING COLUMNS
        pickNumberColumn = new TableColumn("Pick #");
        pickNumberColumn.setCellValueFactory(new PropertyValueFactory<MLBPlayer, Integer>("pickNumber"));
        firstNameColumn = new TableColumn("First");
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<MLBPlayer, String>("firstName"));
        firstNameColumn.setSortable(true);
        lastNameColumn = new TableColumn("Last");
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<MLBPlayer, String>("lastName"));
        lastNameColumn.setSortable(true);
        fantasyTeamColumn = new TableColumn("Team");
        fantasyTeamColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<MLBPlayer, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<MLBPlayer, String> param) {
                if(param.getValue() instanceof FantasyHitter)
                {
                    FantasyHitter h = (FantasyHitter)(param.getValue());
                    return new ReadOnlyObjectWrapper(h.getFantasyTeamName());
                }
                if(param.getValue() instanceof FantasyPitcher)
                {
                    FantasyPitcher p = (FantasyPitcher)param.getValue();
                    return new ReadOnlyObjectWrapper(p.getFantasyTeamName());
                }
                return null;
            }
        });
        contractsColumn = new TableColumn("Contract");
        contractsColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<MLBPlayer, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<MLBPlayer, String> param) {
                if(param.getValue() instanceof FantasyHitter)
                {
                    FantasyHitter h = (FantasyHitter)(param.getValue());
                    return new ReadOnlyObjectWrapper(h.getContractString(h.getContractInt()));
                }
                if(param.getValue() instanceof FantasyPitcher)
                {
                    FantasyPitcher p = (FantasyPitcher)param.getValue();
                    return new ReadOnlyObjectWrapper(p.getContractString(p.getContractInt()));
                }
                return null;
            }
        });
        salaryColumn = new TableColumn("Salary ($)");
        salaryColumn.setCellValueFactory(new PropertyValueFactory<MLBPlayer, String>("Salary"));
        //ADDING COLUMNS TO TABLE
        this.getColumns().addAll(pickNumberColumn,
                                 firstNameColumn,
                                 lastNameColumn,
                                 fantasyTeamColumn,
                                 contractsColumn,
                                 salaryColumn);
        this.setEditable(true);
        this.setMinSize(1200, 600);
    }
    
    public void showPlayers()
    {
        this.setItems(mlbPlayers);
    }
    public void clearPlayers()
    {
        mlbPlayers.clear();
    }
    public ObservableList<MLBPlayer> getDraftedPlayers()
    {
        return mlbPlayers;
    }
    public void setAllDraftedPlayers(ObservableList<MLBPlayer> players)
    {
        if(mlbPlayers == null)
            mlbPlayers = FXCollections.observableArrayList();
        else
            mlbPlayers.clear();
        mlbPlayers.addAll(players);
    }
    public void setAllDraftedPlayers(ObservableList<FantasyHitter> h, ObservableList<FantasyPitcher> p)
    {
        if(mlbPlayers== null)
            mlbPlayers = FXCollections.observableArrayList();
        else
            mlbPlayers.clear();
        mlbPlayers.addAll(h);
        mlbPlayers.addAll(p);
        FXCollections.sort(mlbPlayers, (playerA, playerB) -> playerA.getPickNumber() - playerB.getPickNumber());
    }
    public void loadAllDraftedPlayers()
    {
        mlbPlayers = FXCollections.observableArrayList();
        if(fantasyHitters == null)
            fantasyHitters = FXCollections.observableArrayList();
        if(fantasyPitchers == null)
            fantasyPitchers = FXCollections.observableArrayList();
        mlbPlayers.addAll(fantasyHitters);
        mlbPlayers.addAll(fantasyPitchers);
        FXCollections.sort(mlbPlayers, (playerA, playerB) -> playerA.getPickNumber() - playerB.getPickNumber());
    }
    public ObservableList<FantasyHitter> getFantasyHitters()
    {
        return fantasyHitters;
    }
    public ObservableList<FantasyPitcher> getFantasyPitchers()
    {
        return fantasyPitchers;
    }
    public void setFantasyHitters(ObservableList<FantasyHitter> h)
    {
        if(fantasyHitters == null)
            fantasyHitters = FXCollections.observableArrayList();
        else
            fantasyHitters.clear();
        fantasyHitters.addAll(h);
    }
    public void setFantasyPitchers(ObservableList<FantasyPitcher> p )
    {
        if(fantasyPitchers == null)
            fantasyPitchers = FXCollections.observableArrayList();
        else
            fantasyPitchers.clear();
        fantasyPitchers.addAll(p);
    }
}
        