/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wolfieball.gui;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import wolfieball.mlb.MLBHitter;
import wolfieball.mlb.MLBPitcher;
import wolfieball.mlb.MLBPlayer;

/**
 *
 * @author Tristen
 */
public class MLBPlayersTable extends TableView{
    private TableColumn firstNameColumn;
    private TableColumn lastNameColumn;
    private TableColumn proTeamColumn;
    private TableColumn qualifyingPositionsColumn;
    private ObservableList<MLBHitter> mlbHitters;
    private ObservableList<MLBPitcher> mlbPitchers;
    private ObservableList<MLBPlayer> mlbPlayers;
    private ObservableList<MLBPlayer> currentMLBTeamPlayers;
    public MLBPlayersTable()
    {
        super();
    }
    public void initialize()
    {
        firstNameColumn = new TableColumn("First");
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<MLBPlayer, String>("firstName"));
        firstNameColumn.setSortable(true);
        lastNameColumn = new TableColumn("Last");
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<MLBPlayer, String>("lastName"));
        firstNameColumn.setSortable(true);
        qualifyingPositionsColumn = new TableColumn("Positions");
        qualifyingPositionsColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<MLBPlayer, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<MLBPlayer, String> param) {
                boolean isHitter = param.getValue().getIsHitter();
                if(isHitter)
                {
                    wolfieball.mlb.MLBHitter h = (wolfieball.mlb.MLBHitter)(param.getValue());
                    return new ReadOnlyObjectWrapper(h.getQualifyingPositionString());
                }
                else
                {
                    wolfieball.mlb.MLBPitcher p = (wolfieball.mlb.MLBPitcher)param.getValue();
                    return new ReadOnlyObjectWrapper(p.getRole());
                }
            }
        });
        this.getColumns().addAll(firstNameColumn,
                                 lastNameColumn,
                                 qualifyingPositionsColumn);
        this.setMinSize(1200, 600);
    }
    public void showPlayers()
    {
        this.setItems(mlbPlayers);
    }
    public void clearPlayers()
    {
        mlbPlayers.clear();
    }
    public ObservableList<MLBPlayer> getAllMLBPlayers()
    {
        return mlbPlayers;
    }
    public void setMLBPlayers(ObservableList<MLBPlayer> players)
    {
        if(mlbPlayers == null)
            mlbPlayers = FXCollections.observableArrayList();
        else
            mlbPlayers.clear();
        mlbPlayers.addAll(players);
    }
}
