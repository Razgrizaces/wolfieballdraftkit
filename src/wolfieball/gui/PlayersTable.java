package wolfieball.gui;

import java.util.ArrayList;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.Callback;
import wolfieball.file.JsonDraftFileManager;
import wolfieball.mlb.MLBHitter;
import wolfieball.mlb.MLBPitcher;
import wolfieball.mlb.MLBPlayer;

public class PlayersTable extends TableView
{
    private TableColumn firstNameColumn;
    private TableColumn lastNameColumn;
    private TableColumn proTeamColumn;
    private TableColumn positionsColumn;
    private TableColumn yearOfBirth;
    private TableColumn runsWinsColumn;
    private TableColumn homeRunsSavesColumn;
    private TableColumn RBIStrikeoutsColumn;
    private TableColumn stolenBasesERAColumn;
    private TableColumn battingAverageWHIPColumn;
    private TableColumn estimatedValueColumn;
    private TableColumn notesColumn;
    private ObservableList<MLBHitter> mlbHitters;
    private ObservableList<MLBPitcher> mlbPitchers;
    private ObservableList<MLBPlayer> mlbPlayers;
    private JsonDraftFileManager fileManager = new JsonDraftFileManager();
    
    public PlayersTable()
    {
        super();
    }
    public void initialize()
    {
        //ADDING COLUMNS
        firstNameColumn = new TableColumn("First");
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<MLBPlayer, String>("firstName"));
        firstNameColumn.setSortable(true);
        lastNameColumn = new TableColumn("Last");
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<MLBPlayer, String>("lastName"));
        firstNameColumn.setSortable(true);
        proTeamColumn = new TableColumn("Pro Team");
        proTeamColumn.setCellValueFactory(new PropertyValueFactory<MLBPlayer, String>("team"));
        positionsColumn = new TableColumn("Positions");
        positionsColumn.setCellValueFactory(new Callback<CellDataFeatures<MLBPlayer, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(CellDataFeatures<MLBPlayer, String> param) {
                boolean isHitter = param.getValue().getIsHitter();
                if(isHitter)
                {
                    MLBHitter h = (MLBHitter)(param.getValue());
                    return new ReadOnlyObjectWrapper(h.getQualifyingPosition());
                }
                else
                {
                    MLBPitcher p = (MLBPitcher)param.getValue();
                    return new ReadOnlyObjectWrapper(p.getRole());
                }
            }
        });       yearOfBirth = new TableColumn("Year of Birth");
        yearOfBirth.setCellValueFactory(new PropertyValueFactory<MLBPlayer, String>("yearOfBirth"));
        runsWinsColumn = new TableColumn("R/W");
        runsWinsColumn.setCellValueFactory(new Callback<CellDataFeatures<MLBPlayer, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(CellDataFeatures<MLBPlayer, String> param) {
                boolean isHitter = param.getValue().getIsHitter();
                if(isHitter)
                {
                    MLBHitter h = (MLBHitter)(param.getValue());
                    return new ReadOnlyObjectWrapper(h.getRuns());
                }
                else
                {
                    MLBPitcher p = (MLBPitcher)param.getValue();
                    return new ReadOnlyObjectWrapper(p.getWins());
                }
            }
        });
        homeRunsSavesColumn = new TableColumn("HR/SV");
        homeRunsSavesColumn.setCellValueFactory(new Callback<CellDataFeatures<MLBPlayer, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(CellDataFeatures<MLBPlayer, String> param) {
                boolean isHitter = param.getValue().getIsHitter();
                
                if(isHitter)
                {
                    MLBHitter h = (MLBHitter)(param.getValue());
                    return new ReadOnlyObjectWrapper(h.getHomeRuns());
                }
                else
                {
                    MLBPitcher p = (MLBPitcher)param.getValue();
                    return new ReadOnlyObjectWrapper(p.getSaves());
                }
            }
        });
        RBIStrikeoutsColumn = new TableColumn("RBI/K");
        RBIStrikeoutsColumn.setCellValueFactory(new Callback<CellDataFeatures<MLBPlayer, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(CellDataFeatures<MLBPlayer, String> param) {
                boolean isHitter = param.getValue().getIsHitter();
                
                if(isHitter)
                {
                    MLBHitter h = (MLBHitter)(param.getValue());
                    return new ReadOnlyObjectWrapper(h.getRunsBattedIn());
                }
                else
                {
                    MLBPitcher p = (MLBPitcher)param.getValue();
                    return new ReadOnlyObjectWrapper(p.getStrikeouts());
                }
            }
        });        
        stolenBasesERAColumn = new TableColumn("SB/ERA");
        stolenBasesERAColumn.setCellValueFactory(new Callback<CellDataFeatures<MLBPlayer, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(CellDataFeatures<MLBPlayer, String> param) {
                boolean isHitter = param.getValue().getIsHitter();
                
                if(isHitter)
                {
                    MLBHitter h = (MLBHitter)(param.getValue());
                    return new ReadOnlyObjectWrapper(h.getStolenBases());
                }
                else
                {
                    MLBPitcher p = (MLBPitcher)param.getValue();
                    return new ReadOnlyObjectWrapper(p.getERA());
                }
            }
        });   
        battingAverageWHIPColumn = new TableColumn("BA/WHIP");
        battingAverageWHIPColumn.setCellValueFactory(new Callback<CellDataFeatures<MLBPlayer, String>, ObservableValue<String>>()
        {
            @Override
            public ObservableValue<String> call(CellDataFeatures<MLBPlayer, String> param) {
                boolean isHitter = param.getValue().getIsHitter();
                
                if(isHitter)
                {
                    MLBHitter h = (MLBHitter)(param.getValue());
                    return new ReadOnlyObjectWrapper(h.getBattingAverage());
                }
                else
                {
                    MLBPitcher p = (MLBPitcher)param.getValue();
                    return new ReadOnlyObjectWrapper(p.getWHIP());
                }
            }
        }); 
        estimatedValueColumn = new TableColumn("Estimated Value");
        estimatedValueColumn.setCellValueFactory(new PropertyValueFactory<MLBPlayer, String>("estimatedValue"));
        notesColumn = new TableColumn("Notes");
        notesColumn.setCellValueFactory(new PropertyValueFactory<MLBPlayer, String>("notes"));
        notesColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        notesColumn.setOnEditCommit(
            new EventHandler<CellEditEvent<MLBPlayer, String>>() 
            {
                @Override
                public void handle(CellEditEvent<MLBPlayer, String> t)
                {
                    t.getTableView().getItems().
                            get(t.getTablePosition().getRow()).
                            setNotes(t.getNewValue());
                }
            });
        //ADDING COLUMNS TO TABLE
        this.getColumns().addAll(firstNameColumn,
                                         lastNameColumn,
                                         proTeamColumn,
                                         positionsColumn,
                                         yearOfBirth,
                                         runsWinsColumn,
                                         homeRunsSavesColumn,
                                         RBIStrikeoutsColumn,
                                         stolenBasesERAColumn,
                                         battingAverageWHIPColumn,
                                         estimatedValueColumn);
        this.setEditable(true);
        this.setMinSize(1200, 600);
        
        notesColumn.setEditable(true);
        this.getColumns().add(notesColumn);
    }
    public void loadPlayers(String draftPathH, String draftPathP)
    {
        mlbHitters = fileManager.loadMLBHitters(draftPathH);
        mlbPitchers = fileManager.loadMLBPitchers(draftPathP);
        mlbPlayers = FXCollections.observableArrayList(mlbHitters);
        mlbPlayers.addAll(mlbPitchers);
    }
    public void loadPlayers()
    {
        mlbPlayers = FXCollections.observableArrayList();
        mlbPlayers.addAll(mlbHitters);
        mlbPlayers.addAll(mlbPitchers);
    }
    public void showPlayers()
    {
        this.setItems(mlbPlayers);
    }
    public void clearPlayers()
    {
        mlbPlayers.clear();
    }
    public ObservableList<MLBPlayer> getAllMLBPlayers()
    {
        return mlbPlayers;
    }
    public ObservableList<MLBPlayer> getFilteredMLBPlayers(String filter)
    {
        clearPlayers();
        for(MLBHitter h: mlbHitters)
        {
                if(filter.equals("CI"))
                {
                    if(h.getQualifyingPosition().equals("1B")
                     ||h.getQualifyingPosition().equals("3B"))
                        mlbPlayers.add((MLBPlayer)h);
                }
                if(filter.equals("MI"))
                {
                    if(h.getQualifyingPosition().equals("SS")
                     ||h.getQualifyingPosition().equals("2B"))
                        mlbPlayers.add((MLBPlayer)h);
                }
                if(h.isQualifyingPosition(filter)
                        &&!filter.equals("P")
                        ||filter.equals("All")
                        ||filter.equals("U"))
                {
                    mlbPlayers.add((MLBPlayer)h);
                }
        }
        for(MLBPitcher p: mlbPitchers)
        {
            if(filter.equals("P")||filter.equals("All"))
            {
                mlbPlayers.add((MLBPlayer)p);
            }
        }
        return mlbPlayers;
    }
    public ObservableList<MLBHitter> getMLBHitters()
    {
        return mlbHitters;
    }
    public ObservableList<MLBPitcher> getMLBPitchers()
    {
        return mlbPitchers;
    }
    public void setMLBPlayers(ObservableList<MLBPlayer> p)
    {
        if(mlbPlayers == null)
            mlbPlayers = FXCollections.observableArrayList();
        else
            mlbPlayers.clear();
        mlbPlayers.addAll(p);
    }
    public void setMLBPlayers(ObservableList<MLBHitter> h, ObservableList<MLBPitcher> p)
    {
        if(mlbPlayers== null)
            mlbPlayers = FXCollections.observableArrayList();
        else
            mlbPlayers.clear();
        mlbPlayers.addAll(h);
        mlbPlayers.addAll(p);
    }
    public void setMLBHitters(ObservableList<MLBHitter> h)
    {
        if(mlbHitters == null)
            mlbHitters = FXCollections.observableArrayList();
        else
            mlbHitters.clear();
        mlbHitters.addAll(h);
    }
    public void setMLBPitchers(ObservableList<MLBPitcher> p )
    {
        if(mlbPitchers == null)
            mlbPitchers = FXCollections.observableArrayList();
        else
            mlbPitchers.clear();
        mlbPitchers.addAll(p);
    }
}